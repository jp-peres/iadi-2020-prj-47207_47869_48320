import React from 'react'
import styled from 'styled-components'


const StyledFooter = styled.footer`
text-align: center;
color:white;
height:8.1vh;
width:100vw;
display:flex;
justify-content:center;
align-items:center;
`;

const Footer: React.FC = () => {
    return (
        <StyledFooter className="bg-dark">
            CIAI 2020 - João Peres, João Oliveira e Tiago Bica
        </StyledFooter>
    )
};

export default Footer
