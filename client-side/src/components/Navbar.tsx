import React, { useEffect, useState } from 'react'
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import { CgProfile } from 'react-icons/cg';
import { FaGraduationCap, FaRegEdit } from 'react-icons/fa';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Login from './Login/Login'
import { ParseJwt } from '../api/myutils';
import { Link } from 'react-router-dom'
import './Navbar.css'
import default_navi, { navi_admin, navi_reviewer, navi_student } from '../assets/navbar/Items-nav'
import { UserProps } from '../App';


type LoggedProps = {
    triggerLog: React.Dispatch<React.SetStateAction<boolean>>;
    triggerUser: React.Dispatch<React.SetStateAction<UserProps | null>>,
    user: string | null;
    role: string;
}
const Logged = ({ triggerLog, user, role, triggerUser }: LoggedProps) => {

    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleMenu = (event: React.MouseEvent<HTMLElement>) => setAnchorEl(event.currentTarget);
    const handleClose = () => setAnchorEl(null);

    const doLogout = () => {
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        let requestOptions: RequestInit = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow'
        };
        fetch("http://localhost:8080/logout", requestOptions)
            .then(response => {
                window.localStorage.clear();
                triggerLog(false);
                triggerUser(null);
            })
            .catch(error => console.log('error', error));
    }

    return (
        <>

            <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
            >
                <p className="username">Welcome, {user}</p>
                {
                    (role === "ROLE_ADMIN") ?
                        <CgProfile color="white" /> :
                        (role === "ROLE_STUDENT") ?
                            <FaGraduationCap color="white" /> :
                            <FaRegEdit color="white" />
                }
            </IconButton>
            <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
            >
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={doLogout}>Logout</MenuItem>
            </Menu>
        </>
    )
}

type NavProps = {
    log_val: boolean;
    logFun: React.Dispatch<React.SetStateAction<boolean>>;
    user: UserProps | null;
    userFun: React.Dispatch<React.SetStateAction<UserProps | null>>;
    lt: boolean;
    ltFun: React.Dispatch<React.SetStateAction<boolean>>;
}
const NavBar = ({ log_val, logFun, user, userFun, lt, ltFun }: NavProps) => {

    const loadUser = () => {
        const token = window.localStorage.getItem("token");
        if (token !== null) {
            const { authorities, sub } = ParseJwt(token);
            const authority = authorities[0].authority;
            userFun({ authority, sub });
        }
    }

    let nav_items = default_navi;
    const setNav = (userData: any) => {
        if (userData === null) {
            return default_navi;
        }
        else {
            switch (userData!!.authority) {
                case "ROLE_ADMIN":
                    nav_items = navi_admin;
                    break;
                case "ROLE_STUDENT":
                    nav_items = navi_student;
                    break;
                case "ROLE_REVIEWER":
                    /*if (evalCPanel())
                      nav_items = navi_cpanel;
                    else*/
                    nav_items = navi_reviewer;
                    break;
                default:
                    nav_items = default_navi;
                    break;
            }
            return nav_items;
        }
    }

    useEffect(() => {
        if (lt) {
            loadUser();
            logFun(true);
        } else {
            logFun(false);
        }
    }, [lt]);

    return (
        <Navbar className="bg-dark justify-content-between" variant="dark" expand="md">

            <Navbar.Brand>
                <Link to="/" style={{ color: "#fff", textDecoration: "none" }}> Grant Management</Link>
            </Navbar.Brand>
            {(log_val) ?
                <Logged triggerLog={ltFun} triggerUser={userFun} user={user!!.sub} role={user!!.authority} />
                :
                <Login triggerLog={ltFun} />
            }

            <Navbar.Toggle />
            <Navbar.Collapse className="flex-grow-0" >
                <Nav className="ml-auto">
                    {setNav(user).slice(0).reverse().map((item) => {
                        return (
                            <Nav.Link key={item.id}><Link to={item.href} style={{ color: "#fff" }}>{item.name.toUpperCase()}</Link></Nav.Link>
                        );
                    })}
                </Nav>
            </Navbar.Collapse>

        </Navbar>
    )
};

export default NavBar