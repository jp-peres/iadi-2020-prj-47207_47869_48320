import React, { useEffect, useState } from 'react';
import { StudentApiFactory, StudentDTO } from '../api/api';
import { ReqHeaders } from '../api/myutils';

function ListStudents() {
    const [error, setError] = useState('');
    const [studs, setStuds] = useState([] as unknown as StudentDTO[] | undefined);
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        StudentApiFactory(undefined, undefined, undefined).getAllStudents(undefined, undefined, undefined, { headers: ReqHeaders() })
            .then(resp => {
                setIsLoaded(true);
                setStuds(resp.content)
            }, err => {
                setIsLoaded(true);
                setError("Failed")
            })

    }, []);

    if (error) {
        return <div>Error: {error} </div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <ul>
                {studs!!.map(s => <li key={s.id}>{s.name} : {s.email}</li>)}
            </ul>
        );
    }
}
export default ListStudents;
