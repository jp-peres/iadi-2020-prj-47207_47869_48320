import React from 'react'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import ListGroup from 'react-bootstrap/ListGroup'
import { GrantCallDTO } from '../api/api';
import './CallCard.css'
const CallCard: React.FC<GrantCallDTO> = (info: GrantCallDTO) => {
 return (
  <Card className="text-center" style={{ width: '18rem', backgroundColor: '#C7C1C1', borderRadius: '50px' }}>
   <Card.Body>
    <Card.Title >{info.title} {info.id}</Card.Title>
    <Card.Subtitle style={{ color: '#008000' }} className="mb-2">
     Funding: {info.funding}€
     </Card.Subtitle>
    <Card.Text>
     {info.description}
    </Card.Text>
    <Card.Subtitle style={{ fontSize: '10px', color: 'grey' }} className="mb-2">
     Created: {info.openDate}
    </Card.Subtitle>
    <ListGroup variant="flush" style={{ height: '150px', overflowY: 'scroll', overflowX: 'hidden', marginBottom: '1rem', borderRadius: '50px' }}>
     {info.reqItems.map((reqi) => {
      return (
       <ListGroup.Item key={reqi.id}>{reqi.itemName}</ListGroup.Item>
      );
     })}
    </ListGroup>
    <Card.Subtitle style={{ color: '#FF0000' }} className="mb-2">
     Deadline: {info.deadline}
    </Card.Subtitle>
    <Button style={{ boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)', borderRadius: '50px' }} variant="dark">Fill Requirements</Button>
   </Card.Body>
  </Card>
 )
}

export default CallCard;
