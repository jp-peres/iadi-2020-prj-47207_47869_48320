import React from 'react';
import { withStyles, Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }),
)(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
  root: {
    width: 400
  }
});

const getHeaders = (list: any[]) => {
  let val = list[0];
  return Object.keys(val);
}


type TableProps = {
  list: any[] | null;
  loaded: boolean;
  storeName: string;
}

export default function CustomizedTable({ list, loaded, storeName }: TableProps) {
  const classes = useStyles();

  if (loaded) {
    let rows: any[] = [];

    if (list!!.length === 0) {
      let test = window.localStorage.getItem(storeName);
      let obj = JSON.parse(test!!);
      if (obj.arr.length !== 0)
        rows = getHeaders(obj.arr);
      else
        return <div>No results...</div>
    } else
      rows = getHeaders(list!!);
    return (
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              {rows.map((myRow: any) => {
                return <StyledTableCell align="center">{myRow}</StyledTableCell>
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {list!!.map((row) => {
              return <StyledTableRow hover key={row.id}>{Object.keys(row).map((k) => {
                return <StyledTableCell align="center">{row[k]}</StyledTableCell>
              })}</StyledTableRow>
            })}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
  else {
    return (<div>No results...</div>);
  }
}
