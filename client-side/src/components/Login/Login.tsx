import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form'
import FormGroup from 'react-bootstrap/FormGroup'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import './Login.css'

type LoginProps = { triggerLog: React.Dispatch<React.SetStateAction<boolean>> }
const Login = ({ triggerLog }: LoginProps) => {
    const [show, setShow] = useState(false);
    const [showErr, setErr] = useState(true);

    const handleClose = () => {
        setShow(false);
    }

    const handleModalShow = () => {
        setShow(true);
    }

    const doLogin = (formAttr: any) => {
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        let user = formAttr.target[0].value;
        let pass = formAttr.target[1].value;

        let raw = JSON.stringify({ "username": user, "password": pass });

        let requestOptions: RequestInit = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("http://localhost:8080/login", requestOptions)
            .then(response => {
                if (response.status >= 200 && response.status <= 299) {
                    let res = response.headers.get("Authorization");
                    window.localStorage.setItem("token", res!!);
                    triggerLog(true);
                    setErr(true);
                }
                else {
                    setErr(false);
                }
            });
        formAttr.preventDefault();
    }

    return (
        <>
            <Breadcrumb bsPrefix="breadcrumb p-2 my-0 bg-transparent">
                <Breadcrumb.Item active >sign up</Breadcrumb.Item>
                <Breadcrumb.Item onClick={handleModalShow}>login</Breadcrumb.Item>
            </Breadcrumb>
            <Modal size="sm" centered={true} show={show} onHide={handleClose}>
                <Modal.Header bsPrefix="login-header" closeButton>
                    <Modal.Title>Login Form</Modal.Title>
                </Modal.Header>
                <Modal.Body bsPrefix="login-body">
                    <Form method="POST" onSubmit={doLogin}>
                        <FormGroup>
                            <Form.Control size="sm" required={true} name="user" type="text" placeholder="Username" />
                            <Form.Control size="sm" required={true} name="pass" type="password" placeholder="Password" />
                        </FormGroup>
                        <FormGroup>
                            <p hidden={showErr} style={{ color: "red" }}>Wrong credentials... try again.</p>
                            <Button type="submit" variant="dark">
                                LOGIN
                        </Button>
                            <Button variant="light" onClick={handleClose}>
                                CANCEL
                        </Button>
                        </FormGroup>
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    );
}
export default Login;