import React, { useEffect, useState } from 'react'
import { StudentApiFactory } from '../api/api';
import { ReqHeaders } from '../api/myutils';
import CustomizedTable from '../components/Table/CustomizedTable';
import './Main.css';

const Main = () => {
    const [applications, setApplications] = useState<any[]>([]);
    const [loaded, setIsLoaded] = useState(false);

    useEffect(() => {
        StudentApiFactory(undefined, undefined, undefined).getAllStudents(undefined, undefined, undefined, { headers: ReqHeaders() })
            .then(resp => {
                let cont = resp!!.content;
                setApplications(cont!!);
                let obj = {
                    arr: cont!!
                }
                let arr = JSON.stringify(obj);
                window.localStorage.setItem("apps", arr);
                setIsLoaded(true);
            }, err => {
                console.log(err);
            })

    }, []);

    return (
        <div className="App" style={{ backgroundColor: 'whitesmoke', height: '80vh', width: '100vw' }}>
            <CustomizedTable list={applications!!} loaded={loaded} storeName="apps" />
        </div>
    )
}
export default Main;
