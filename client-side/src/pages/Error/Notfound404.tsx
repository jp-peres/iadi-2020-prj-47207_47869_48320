import React from 'react'
import Container from 'react-bootstrap/Container'
import { Link } from 'react-router-dom';

const Notfound404 = () => {
 const message: String = '>page not found!<';
 const ecode: Number = 404;
 const btn_txt: String = 'Sweet Home';

 return (
  <Container className="text-center" fluid >
   <h1>{ecode}</h1>
   <h6>{message.toUpperCase()}</h6>
   <br />
   <Link to='/'> {btn_txt}</Link>
  </Container>
 )
}

export default Notfound404
