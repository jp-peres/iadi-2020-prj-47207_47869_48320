import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
//import { /* GrantCallApiFetchParamCreator, */GrantCallDTO, RequestItemDTO } from '../api/api'
import CallCard from '../components/CallCard'
import { GrantCall } from '../assets/navbar/grantcall-example'

function CreateApp() {
 /*const [calls, setCalls] = useState<any[]>([]);
 const [loaded, setIsLoaded] = useState(false);

 useEffect(() => {
  GrantCallApiFetchParamCreator(undefined, undefined, undefined).getAllGrantCallsUsingGET(undefined, undefined, undefined, { headers: ReqHeaders() })
   .then(resp => {
    let cont = resp!!.content;
    setCalls(cont!!);
    setIsLoaded(true);
   }, err => {
    console.log(err);
   })

 }, []);*/
 return (
  <Container >
   < Row >
    <Col style={{ margin: '0.5rem 0rem' }}>
     List of all grant calls
     {/* <CustomizedTable list={applications!!} loaded={loaded} /> */}
    </Col>
    <Col style={{ margin: '0.5rem 0rem' }}>
     <CallCard {...GrantCall} />
    </Col>
   </Row>
  </ Container>
 )
}

export default CreateApp


/*  <div style={{
    backgroundColor: 'whitesmoke',
    height: '80vh', width: '100vw', paddingInline: '12rem',
    display: 'flex', justifyContent: 'flex-end', alignItems: 'center'
   }}>
    <CallCard {...grantCall} />
   </div> */