import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import { req_items } from '../assets/navbar/grantcall-example'

function EditApp() {
 return (
  <Container >
   < Row >
    <Col style={{ margin: '0.5rem 0rem' }}>
     List of Unsubmitted Applications
    </Col>
    <Col style={{ margin: '0.5rem 0rem' }}>
     <Form>
      {req_items.map((reqi) => {
       return (
        <Form.Group key={reqi.id}>
         <Form.File label={reqi.itemName} />
        </Form.Group>
       );
      })}

     </Form>
    </Col>
   </Row>
  </ Container>
 )
}

export default EditApp
