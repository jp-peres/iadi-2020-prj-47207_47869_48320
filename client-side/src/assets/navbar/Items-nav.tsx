const nav_items = [
    {
        id: 0,
        name: 'help',
        href: 'help'
    },
    {
        id: 1,
        name: 'grantcall list',
        href: 'gcl'
    }
];

export const navi_student = [
    {
        id: 0,
        name: 'help',
        href: 'help'
    },
    {
        id: 1,
        name: 'grantcall list',
        href: 'gcl'
    },
    {
        id: 2,
        name: 'submissions list',
        href: 'sl'
    },
    {
        id: 3,
        name: 'create application',
        href: '/application/create'
    },
    {
        id: 4,
        name: 'edit application',
        href: '/application/edit'
    },
    {
        id: 5,
        name: 'evaluated list',
        href: 'el'
    }
];

export const navi_reviewer = [
    {
        id: 0,
        name: 'help',
        href: 'help'
    },
    {
        id: 1,
        name: 'evaluation panels',
        href: 'ep'
    },
    {
        id: 2,
        name: 'panel information',
        href: 'pi'
    },
    {
        id: 3,
        name: 'create evaluation',
        href: 'ce'
    },
    {
        id: 4,
        name: 'edit evaluation',
        href: 'ee'
    }
];

export const navi_cpanel = [
    {
        id: 0,
        name: 'help',
        href: 'help'
    },
    {
        id: 1,
        name: 'evaluation panels',
        href: 'ep'
    },
    {
        id: 2,
        name: 'panel information',
        href: 'pi'
    },
    {
        id: 3,
        name: 'create evaluation',
        href: 'ce'
    },
    {
        id: 4,
        name: 'edit evaluation',
        href: 'ee'
    },
    {
        id: 5,
        name: 'decide applications',
        href: 'da'
    }
];

export const navi_admin = [
    {
        id: 0,
        name: 'grantcall list',
        href: 'gcl'
    },
    {
        id: 1,
        name: 'submissions list',
        href: 'sl'
    },
    {
        id: 2,
        name: 'create application',
        href: '/application/create'
    },
    {
        id: 3,
        name: 'edit application',
        href: '/application/edit'
    },
    {
        id: 4,
        name: 'evaluated list',
        href: 'el'
    },
    {
        id: 5,
        name: 'evaluation panels',
        href: 'ep'
    },
    {
        id: 6,
        name: 'panel information',
        href: '#pi'
    },
    {
        id: 7,
        name: 'create evaluation',
        href: 'ce'
    },
    {
        id: 8,
        name: 'edit evaluation',
        href: 'ea'
    },
    {
        id: 9,
        name: 'decide applications',
        href: 'da'
    }
];

export default nav_items;
