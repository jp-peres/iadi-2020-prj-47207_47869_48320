import { /* GrantCallApiFetchParamCreator, */GrantCallDTO, RequestItemDTO } from '../../api/api'

export const req_items: RequestItemDTO[] = [
 {
  id: 0,
  itemName: 'CV',
  required: true,
  type: 'type'
 },
 {
  id: 1,
  itemName: 'Notes',
  required: false,
  type: 'type'
 },
 {
  id: 2,
  itemName: 'Documents',
  required: false,
  type: 'type'
 },
 {
  id: 3,
  itemName: 'Something',
  required: true,
  type: 'type'
 },
 {
  id: 4,
  itemName: 'Else',
  required: false,
  type: 'type'
 }
]
export const GrantCall: GrantCallDTO = {
 deadline: '20/05/2020',
 description: 'Doloribus eligendi vel atque cupiditate sequi harum odio sint alias deserunt cumque similique ducimus, voluptas vitae corporis nihil obcaecati id maxime.',
 funding: 1000,
 id: 8,
 idSponsor: 1,
 openDate: '20/11/2020 00:30',
 reqItems: req_items,
 requirements: '12º done and master of Kotlin',
 title: 'GrantCall'
}