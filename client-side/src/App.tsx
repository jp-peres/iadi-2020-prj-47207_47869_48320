import React, { useState } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import NavBar from './components/Navbar';
import Footer from './components/Footer';
import Home from './pages/Main';

import NotFound from './pages/Error/Notfound404'
import CreateApp from './pages/CreateApp';
import EditApp from './pages/EditApp';



export type UserProps = { authority: string, sub: string };
function App() {

  const [logged, setLogged] = useState(false);
  const [userData, setUser] = useState<null | UserProps>(null);
  const [listen, setListen] = useState(false);

  return (
    <Router>
      <NavBar log_val={logged} logFun={setLogged} user={userData} userFun={setUser} lt={listen} ltFun={setListen} />
      <Switch>
        <Route exact path='/'>
          <Home />
        </Route>
        <Route path='/application/create'>
          <CreateApp />
        </Route>
        <Route path='/application/edit'>
          <EditApp />
        </Route>
        <Route path='*'>
          <NotFound />
        </Route>

      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
