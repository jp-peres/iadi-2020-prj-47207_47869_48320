package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.FORBIDDEN
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.SERVER_ERROR
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.UNAUTHORIZED
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Application.Companion.APPLICATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Evaluation.Companion.EVALUATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.EvaluationPanel.Companion.EVALUATION_PANEL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.GrantCall.Companion.GRANTCALL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Reviewer.Companion.REVIEWER_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.AllowedForDeletionReviewer
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.AllowedForGetAllReviewers
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.AllowedForGetReviewer

@Api(value="Reviewer", description = "Reviewer API", tags=["Reviewer"])
@RequestMapping("$RESOURCE/$REVIEWER_PATH")
interface Reviewer {

    companion object {
        const val REVIEWER_PATH: String = "reviewers"
    }

    @ApiOperation(value = "Retrieves all reviewers from the system", nickname="getAllReviewers")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns all reviewers"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetAllReviewers
    @GetMapping("")
    fun getAllReviewers(@RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                        @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                        @RequestParam(required=false) sorting:List<String>?):Page<ReviewerDTO>


    @ApiOperation(value = "Retrieves a reviewer with given id", nickname="getReviewer")
    @ApiResponses(value = [
        ApiResponse(code = 200,message = "Returns the reviewer with specified id"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404,message = "Reviewer with specified id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetReviewer
    @GetMapping("{id}")
    fun getReviewer(@PathVariable id:Long): ReviewerDTO?


    @ApiOperation(value = "Deletes a reviewer with given id", nickname="removeReviewer")
    @ApiResponses(value = [
        ApiResponse(code=204,message = "Deleted the reviewer successfully"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code=404,message = "Reviewer with id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowedForDeletionReviewer
    @DeleteMapping("{id}")
    fun removeReviewer(@PathVariable id:Long)


    @ApiOperation("Creates an evaluation to a grant application")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Successfully creates a new evaluation"),
        ApiResponse(code=401, message = UNAUTHORIZED),
        ApiResponse(code=403, message = FORBIDDEN),
        ApiResponse(code=500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("{id}/$EVALUATION_PATH", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun submitEvaluation(@PathVariable id:Long, @RequestBody evaluation: EvaluationDTO): EvaluationDTO?

    @ApiOperation(value = "Retrieves all existing Applications belonging to a Reviewer's Evaluation Panel", nickname="getAllApplicationsFromPanelGrantCall")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns all applications from the grantCall with id"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "No GrantCall was found with the specified id."),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetAllReviewers
    @GetMapping("{idRev}/$EVALUATION_PANEL_PATH/{idPanel}/$APPLICATION_PATH")
    fun getAllApplicationsFromPanel(@PathVariable idRev:Long, @PathVariable idPanel:Long,
                                    @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                                    @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                                    @RequestParam(required=false) sorting:List<String>?):Page<ApplicationDTO>

    @ApiOperation(value = "Retrieves a GrantApplication with specified id that belongs to the Reviewer's Evaluation Panel", nickname="getApplicationFromPanel")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns the GrantApplication with specified id."),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "No Reviewer/Panel/Application was found with the specified id."),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetAllReviewers
    @GetMapping("{idRev}/$EVALUATION_PANEL_PATH/{idPanel}/$APPLICATION_PATH/{idGA}")
    fun getApplicationFromPanel(@PathVariable idRev:Long, @PathVariable idPanel:Long, @PathVariable idGA:Long):ApplicationDTO?

    @ApiOperation(value = "Retrieves all existing evaluations made by a Reviewer", nickname="getAllEvaluationsFromReviewer")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns all evaluations from the grantCall with id"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "No GrantCall was found with the specified id."),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetAllReviewers
    @GetMapping("{idRev}/$EVALUATION_PATH")
    fun getAllEvaluationsFromReviewer(@PathVariable idRev:Long,
                                    @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                                    @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                                    @RequestParam(required=false) sorting:List<String>?):Page<EvaluationDTO>

    @ApiOperation(value = "Retrieves a specific evaluation that was made by the reviewer with id", nickname="getEvaluationFromReviewer")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns all evaluations from the grantCall with id"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "No GrantCall was found with the specified id."),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetAllReviewers
    @GetMapping("{idRev}/$EVALUATION_PATH/{idEval}")
    fun getEvaluationFromReviewer(@PathVariable idRev:Long, @PathVariable idEval:Long):EvaluationDTO?
}