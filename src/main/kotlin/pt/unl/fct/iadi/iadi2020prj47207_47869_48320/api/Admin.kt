package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.FORBIDDEN
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.UNAUTHORIZED
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.SERVER_ERROR
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Admin.Companion.ADMIN_PATH
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.AuthUserDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.AuthUserDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.AllowedForAdminOperations


@Api(value= ADMIN_PATH, description = "Admin API", tags=["Admin"])
@RequestMapping("$RESOURCE/$ADMIN_PATH")
interface Admin {
    companion object {
        const val ADMIN_PATH: String = "admin"
    }
    @ApiOperation(value = "Creates a new AuthUser.", nickname="createAuthUser")
    @ApiResponses(value = [
        ApiResponse(code=201,message = "AuthUser created successfully"),
        ApiResponse(code=401,message = UNAUTHORIZED),
        ApiResponse(code=403,message = FORBIDDEN),
        ApiResponse(code=409,message = "Theres an existing application already with the same id"),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.CREATED)
    @AllowedForAdminOperations
    @PostMapping("",consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun createAuthUser(@RequestBody user: AuthUserDTO)

    @ApiOperation(value = "Get all AuthUser usernames", nickname="getAllAuthUsers")
    @ApiResponses(value = [
        ApiResponse(code=200,message = "Returns list of usernames"),
        ApiResponse(code=401,message = UNAUTHORIZED),
        ApiResponse(code=403,message = FORBIDDEN),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForAdminOperations
    @GetMapping("all")
    fun getAllAuthUsers(@RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                        @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                        @RequestParam(required=false) sorting:List<String>?): Page<AuthUserDAO>

    @ApiOperation(value = "Checks if a AuthUser exists", nickname="getApplication")
    @ApiResponses(value = [
        ApiResponse(code=200,message = "Boolean deciding if it exists"),
        ApiResponse(code=401,message = UNAUTHORIZED),
        ApiResponse(code=403,message = FORBIDDEN),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForAdminOperations
    @GetMapping("")
    fun authUserExists(@RequestParam username:String): Boolean

    @ApiOperation(value = "Deletes a AuthUser from the system", nickname="deleteAuthUser")
    @ApiResponses(value = [
        ApiResponse(code=204,message = "AuthUser was removed successfully"),
        ApiResponse(code=401,message = UNAUTHORIZED),
        ApiResponse(code=403,message = FORBIDDEN),
        ApiResponse(code=404,message = "The AuthUser with id doesnt exist"),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowedForAdminOperations
    @DeleteMapping("")
    fun deleteAuthUser(@RequestParam username:String)
}