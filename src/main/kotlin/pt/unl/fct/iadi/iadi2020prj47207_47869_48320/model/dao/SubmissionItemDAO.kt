package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao

import javax.persistence.*

@Entity
data class SubmissionItemDAO(@Id @GeneratedValue(strategy = GenerationType.TABLE) val id:Long, val submissionItemValue:String,
                             val itemDataType:String,
                             @ManyToOne @JoinColumn(name="fk_app", nullable=false)
                             val app: ApplicationDAO?)
