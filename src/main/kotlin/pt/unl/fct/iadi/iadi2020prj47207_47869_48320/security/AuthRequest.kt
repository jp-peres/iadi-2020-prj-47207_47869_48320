package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security

class AuthRequest {
    private var username:String = ""
    private var password:String = ""

    constructor(){}
    constructor(username: String,password: String){
        this.username = username
        this.password = password
    }

    fun getUsername() = this.username

    fun setUsername(username:String){
        this.username = username
    }

    fun getPassword() = this.password

    fun setPassword(password:String) {
        this.password = password
    }
}