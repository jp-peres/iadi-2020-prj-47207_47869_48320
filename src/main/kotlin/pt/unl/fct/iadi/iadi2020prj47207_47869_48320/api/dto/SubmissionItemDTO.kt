package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.GrantCallDAO
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

data class SubmissionItemDTO(val id:Long, val submissionItemValue:String, val itemDataType:String, val id_app:Long)