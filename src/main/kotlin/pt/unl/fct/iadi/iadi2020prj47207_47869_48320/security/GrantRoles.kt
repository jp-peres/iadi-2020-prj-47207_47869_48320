package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security

enum class GrantRoles {
    STUDENT,
    REVIEWER,
    SPONSOR,
    ADMIN
}