package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.StudentDAO

data class StudentDTO(override val id: Long, override val name: String, override val email: String, override val idInst: Long, override val address: String, override val phoneNumber: String): UserDTO {
    override val type: String = STUDENT_TYPE
    companion object{
        const val STUDENT_TYPE:String = "STUDENT"
    }
    constructor(st:StudentDAO):this(st.id,st.name,st.email, st.inst.idInst,st.address,st.phoneNumber)
}