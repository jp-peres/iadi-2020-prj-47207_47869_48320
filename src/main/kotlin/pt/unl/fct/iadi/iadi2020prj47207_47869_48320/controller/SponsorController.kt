package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.controller

import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Sponsor
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationPanelDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.GrantCallDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.SponsorDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.SponsorService


@RestController
class SponsorController(val sponsorService:SponsorService):Sponsor {

    override fun getSponsors(pageNumber: Int, pageSize: Int, sorting: List<String>?): Page<SponsorDTO> =
            sponsorService.getSponsors(pageNumber,pageSize,sorting)

    override fun getSponsor(id: Long): SponsorDTO? = sponsorService.getSponsor(id)

    override fun getSponsorGrantCalls(id: Long, pageNumber: Int, pageSize: Int, sorting: List<String>?): Page<GrantCallDTO> =
            sponsorService.getSponsorGrantCalls(id,pageNumber,pageSize,sorting)

    override fun createSponsor(sponsor:SponsorDTO,password:String): SponsorDTO? =
            sponsorService.createSponsor(sponsor,password)

    override fun updateSponsor(id: Long, sponsor: SponsorDTO) =
            sponsorService.updateSponsor(id,sponsor)

    override fun deleteSponsor(id: Long) = sponsorService.deleteSponsor(id)

    override fun createGrantCall(id: Long, grantCall: GrantCallDTO,
                                 evalPanel: EvaluationPanelDTO,
                                 reviewers: List<Long>): GrantCallDTO? =
            sponsorService.createGrantCall(id,grantCall,evalPanel,reviewers)
}