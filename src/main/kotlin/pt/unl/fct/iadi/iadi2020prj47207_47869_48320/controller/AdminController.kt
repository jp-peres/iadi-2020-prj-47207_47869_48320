package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.controller

import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Admin
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.AuthUserDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.AlreadyExistsException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.AuthUserDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.AuthUserRepository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.AllowedForAdminOperations
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.PageableBuilder
import javax.transaction.Transactional

@RestController
class AdminController(val authUserRepository: AuthUserRepository) : Admin {

    @Transactional
    override fun createAuthUser(user: AuthUserDTO) {
        if (authUserRepository.existsByUsername(user.username))
            throw AlreadyExistsException("User Already Exists", "User with '${user.username} already exists.'")
        else {
            val newUser = AuthUserDAO(user.username, user.password, user.active, user.roles)
            authUserRepository.save(newUser)
        }
    }

    override fun getAllAuthUsers(pageNumber: Int, pageSize: Int, sorting: List<String>?): Page<AuthUserDAO> {
        val pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return authUserRepository.findAll(pageable)
    }

    override fun authUserExists(username: String): Boolean {
        return authUserRepository.existsByUsername(username)
    }

    @Transactional
    override fun deleteAuthUser(username: String) {
        if (authUserExists(username)) {
            authUserRepository.deleteByUsername(username)
        } else
            throw NotFoundException("No AuthUser Found", "No AuthUser with specified username '$username' was found.")
    }
}