package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.AlreadyExistsException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.ApplicationDecisionException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ReviewerDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.StudentDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ApplicationDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.AuthUserDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.*
import java.lang.Exception
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.GrantRoles.*

@Service
class UserService(val instRepo: InstitutionRepository,
                  val reviewerRepo: ReviewerRepository,
                  val studentRepo: StudentRepository,
                  val gcRepo: GrantCallRepository,
                  val appRepo: ApplicationRepository,
                  val evalRepo: EvaluationRepository,
                  val authRepository: AuthUserRepository) {

    //TODO: Maybe it's time for revival of separate services? (or not)


    @Transactional
    fun addStudent(stud: StudentDTO, password:String):StudentDTO? {
        val inst = instRepo.findById(stud.idInst).orElseThrow {
            NotFoundException("No Institution Found","No institution with specified id '${stud.idInst}' was found.")
        }

        if (!authRepository.existsByUsername(stud.name))
            authRepository.save(AuthUserDAO(stud.name,password,true, STUDENT.name))
        else
            throw AlreadyExistsException("User Already Exists", "User with name '${stud.name}' already exists.")

        val s = StudentDAO(stud.name,stud.email,inst,stud.address,
                stud.phoneNumber)
        studentRepo.save(s)
        return StudentDTO(s)
    }

    @Transactional
    fun removeStudent(id: Long) {
        try{
            val curr = studentRepo.findById(id).get()
            studentRepo.deleteById(id)
            authRepository.deleteByUsername(curr.name)
        } catch(ex: Exception){
            throw NotFoundException("No Student Found","No student with specified id '$id' was found")
        }
    }

    fun getStudentApplications(id: Long,pageNumber: Int, pageSize: Int,
                               sorting: List<String>?):Page<ApplicationDTO> {
        val pageable: Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return studentRepo.getStudentApplications(id,pageable).map { ApplicationDTO(it) }
    }


    fun getStudentApplication(id: Long, id2: Long): ApplicationDTO {
        if (!studentRepo.existsById(id))
            throw NotFoundException("No Student Found","No student with specified id '$id' was found")
        else {
            return ApplicationDTO(
                    studentRepo.getStudentApplication(id, id2).orElseThrow {
                        NotFoundException("No GrantApplication Found", "No GrantApplication with specified id '$id2' was found")
                }
            )
        }
    }

    fun getStudentApplicationStatus(id: Long, id2: Long): String {
        val app = getStudentApplication(id,id2)
        return app.status
    }


    fun getAllStudents(pageNumber: Int, pageSize: Int, sorting: List<String>?):Page<StudentDTO> {
        val pageable: Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        val res = studentRepo.findAll(pageable)
        return res.map {
            StudentDTO(it)
        }
    }

    fun getStudent(id:Long): StudentDTO? {
        val it = studentRepo.findById(id).orElseThrow{
            NotFoundException("No Student Found","No student with specified id '$id' was found")
        }
        return StudentDTO(it)
    }

    @Transactional
    fun createApplication(app: ApplicationDTO): ApplicationDTO? {
        val gCall = gcRepo.findById(app.grant_id).orElseThrow {
            NotFoundException("No GrantCall was found","No GrantCall with specified id ${app.grant_id} was found.")
        }
        val stud = studentRepo.findById(app.grant_id).orElseThrow {
            NotFoundException("No Student was found","No Student with specified id ${app.grant_id} was found.")
        }
        val created = appRepo.save(ApplicationDAO(app.submissionDate, ApplicationDAO.Status.ON_HOLD,stud,gCall, mutableSetOf()))
        return ApplicationDTO(created)
    }

    @Transactional
    fun addReviewer(rev:ReviewerDTO,password:String):ReviewerDTO? {

        val inst = rev.idInst.let { instRepo.findById(it).orElseThrow{
            NotFoundException("No Institution Found","No institution with specified id $it was found.")
        } }

        if (!authRepository.existsByUsername(rev.name))
            authRepository.save(AuthUserDAO(rev.name,password,true, REVIEWER.name))
        else
            throw AlreadyExistsException("User Already Exists", "User with name '${rev.name}' already exists.")

        val r = reviewerRepo.save(ReviewerDAO(rev.name,rev.email,inst,rev.address,rev.phoneNumber))
        return ReviewerDTO(r)
    }

    @Transactional
    fun removeReviewer(id: Long) {
        try {
            val curr = reviewerRepo.findById(id).get()
            reviewerRepo.deleteById(id)
            authRepository.deleteByUsername(curr.name)
        } catch(ex: Exception){
            throw NotFoundException("No Student Found","No student with specified id '$id' was found.")
        }
    }

    fun getAllReviewers(pageNumber: Int, pageSize: Int, sorting: List<String>?):Page<ReviewerDTO> {
        val pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return reviewerRepo.findAll(pageable).map { ReviewerDTO(it) }
    }
    fun getReviewer(id:Long): ReviewerDTO? {
        val it = reviewerRepo.findById(id).orElseThrow {
            NotFoundException("No Student Found","No student with specified id '$id' was found")
        }
        return ReviewerDTO(it)
    }

    fun getOpenGrantCalls(pageNumber: Int, pageSize: Int,
                          sorting: List<String>?): Page<GrantCallDTO> {
        val pageable: Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return gcRepo.findAllOpenGrantCalls(pageable).map { GrantCallDTO(it) }
    }

    fun getReviewsOfApplicationOfStudent(id: Long, id2: Long,pageNumber: Int,
                                         pageSize: Int,
                                         sorting: List<String>?): Page<EvaluationDTO> {
        val app = appRepo.findById(id).orElseThrow { throw NotFoundException("No Application was found", "No application with specified id '$id' was found.") }
        if(app.status != ApplicationDAO.Status.ACCEPTED && app.status != ApplicationDAO.Status.DENIED)
            throw ApplicationDecisionException("Evaluations not available","Application Decision hasn't been reached yet.")
        val pageable: Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return evalRepo.findByGrantApplication(id,pageable).map { EvaluationDTO(it) }
    }

    fun getEvaluationsFromReviewer(idRev: Long, pageNumber: Int, pageSize: Int, sorting: List<String>?): Page<EvaluationDTO> {
        val pageable: Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return reviewerRepo.getEvaluationsFromReviewer(idRev,pageable).map { EvaluationDTO(it) }
    }

    fun getEvaluationFromReviewer(idRev: Long, idEval: Long): EvaluationDTO? {
        val dao = reviewerRepo.getEvaluationFromReviewer(idRev,idEval).orElseThrow {
            NotFoundException("Evaluation/Reviewer not found","Evaluation/Reviewer with ids '$idRev and $idEval' might have not been found")
        }
        return EvaluationDTO(dao)
    }
}