package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao

import javax.persistence.*

@Entity
@Table(name="evaluation_panel")
data class EvaluationPanelDAO(@OneToOne
                              @JoinColumn(name = "grant_id")
                              val grant: GrantCallDAO,
                              @OneToOne
                              @JoinColumn(name="chairman_id")
                              val idChairman:ReviewerDAO, val academicArea:String,
                              @ManyToMany val reviewers:List<ReviewerDAO>){
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "id") val id:Long = 0
}