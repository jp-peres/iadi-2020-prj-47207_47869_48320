package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.controller

import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.EvaluationPanel
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.EvaluationPanelService

@RestController
class EvaluationPanelController(val evalPanServ: EvaluationPanelService):EvaluationPanel {

    override fun getAllReviewers(idEvalPanel: Long, pageNumber: Int,
                                 pageSize: Int, sorting: List<String>?): Page<ReviewerDTO?> =
            evalPanServ.getAllReviewers(idEvalPanel,pageNumber,pageSize,sorting)

    override fun getAllEvaluations(idEvalPanel: Long, pageNumber: Int,
                                   pageSize: Int, sorting: List<String>?): Page<EvaluationDTO> =
            evalPanServ.getAllEvaluations(idEvalPanel,pageNumber,pageSize,sorting)

    override fun getPanelChair(idEvalPanel: Long): ReviewerDTO? = evalPanServ.getPanelChair(idEvalPanel)
    /***/
    override fun dismantleEvaluationPanel(id: Long) = evalPanServ.dismantleEvaluationPanel(id)

    override fun decideApplication(id: Long, idGA: Long, decision: Boolean) = evalPanServ.decideApplication(id, idGA, decision)

}