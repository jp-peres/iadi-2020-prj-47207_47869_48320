package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.EvaluationDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ReviewerDAO
import java.util.*

@Repository
interface ReviewerRepository:UserRepository<ReviewerDAO> {
    @Query(value="select evals from ReviewerDAO r join r.evaluations evals where r.id =: id")
    fun getEvaluationsFromReviewer(id:Long,pageable: Pageable): Page<EvaluationDAO>

    @Query(value="select eval from ReviewerDAO r join r.evaluations eval where r.id =: id and eval.id =: idEval")
    fun getEvaluationFromReviewer(id:Long, idEval:Long): Optional<EvaluationDAO>
}