
package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.controller

import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Reviewer
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.EvaluationPanelService
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.EvaluationService
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.UserService

@RestController
class ReviewerController(val users: UserService,
                         val evaluations: EvaluationService,
                         val panel:EvaluationPanelService):Reviewer{

    override fun getAllReviewers(pageNumber: Int, pageSize: Int, sorting: List<String>?): Page<ReviewerDTO> =
            users.getAllReviewers(pageNumber,pageSize,sorting)

    override fun getReviewer(id: Long): ReviewerDTO? = users.getReviewer(id)

    override fun removeReviewer(id: Long) = users.removeReviewer(id)

    override fun submitEvaluation(id: Long, evaluation: EvaluationDTO): EvaluationDTO? =
            evaluations.submitEvaluation(evaluation)

    override fun getAllApplicationsFromPanel(idRev: Long, idPanel: Long,
                                             pageNumber: Int, pageSize: Int,
                                             sorting: List<String>?): Page<ApplicationDTO> {
        if(!panel.checkIfReviewerBelongsToPanel(idRev,idPanel))
            throw NotFoundException("Reviewer doesnt belong to Panel","Reviewer with id '$idRev' does not belong to panel id '$idPanel'")
        return panel.getAllApplicationsFromPanel(idPanel, pageNumber, pageSize, sorting)
    }

    override fun getApplicationFromPanel(idRev: Long, idPanel: Long, idGA: Long):ApplicationDTO? {
        if(!panel.checkIfReviewerBelongsToPanel(idRev,idPanel))
            throw NotFoundException("Reviewer doesnt belong to Panel","Reviewer with id '$idRev' does not belong to panel id '$idPanel'")
        return panel.getApplicationFromPanel(idPanel, idGA)
    }

    override fun getAllEvaluationsFromReviewer(idRev: Long, pageNumber: Int, pageSize: Int, sorting: List<String>?): Page<EvaluationDTO> {
        return users.getEvaluationsFromReviewer(idRev,pageNumber,pageSize,sorting)
    }

    override fun getEvaluationFromReviewer(idRev: Long, idEval: Long): EvaluationDTO? {
        return users.getEvaluationFromReviewer(idRev,idEval)
    }
}