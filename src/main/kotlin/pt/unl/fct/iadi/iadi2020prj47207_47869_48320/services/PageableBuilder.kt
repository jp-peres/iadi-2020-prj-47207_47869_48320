package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.domain.Sort.Order
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.InvalidPageParameterException

class PageableBuilder {
    companion object{
        fun build(pageN:Int,pageSize:Int,sorting:List<String>?):Pageable{
            if(pageN < 0)
                throw InvalidPageParameterException("pageNumber is invalid","pageNumber '$pageN' is an invalid value. Use positive values")
            if(pageSize < 0)
                throw InvalidPageParameterException("pageSize is invalid","pageSize '$pageSize' is an invalid value. Use positive values")
            return if (sorting == null)
                PageRequest.of(pageN,pageSize)
            else{
                val sort:Sort = parseSort(sorting)
                PageRequest.of(pageN,pageSize,sort)
            }
        }
        private fun parseSort(sorting:List<String>):Sort{
            val orders = sorting.map {
               var colDir = it.split(".")
               if (colDir[1].toUpperCase() == "ASC")
                   Order.asc(colDir[0])
               else if (colDir[1].toUpperCase() == "DESC")
                   Order.desc(colDir[0])
               else
                   throw InvalidPageParameterException("Sort is invalid","Sort direction from sort '$it' is invalid. (Use: asc,desc,ASC,DESC instead)")
            }
            return Sort.by(orders)
        }
    }
}