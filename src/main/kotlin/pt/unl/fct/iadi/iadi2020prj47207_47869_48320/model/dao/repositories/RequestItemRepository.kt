package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.RequestItemDAO

@Repository
interface RequestItemRepository: JpaRepository<RequestItemDAO, Long> {
    @Query("SELECT r FROM RequestItemDAO r WHERE r.item_name IN ?1")
    fun findAllByItem_name(names: List<String>) : MutableSet<RequestItemDAO>
}