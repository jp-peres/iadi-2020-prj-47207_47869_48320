package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services

import org.springframework.stereotype.Service
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.StudentDTO


@Deprecated("Dead. May be rezzed after change in approach")
@Service
class StudentService {

    var countID : Number = 0
    val students = mutableMapOf<Number, StudentDTO>()

    companion object{
        const val STUDENT_TYPE:String = "STUDENT"
    }

    fun getAllStudents() = emptyList<StudentDTO>()

    fun getStudent(id:Number) = students[id]

    //TODO: how to retrieve applications???
    fun getStudentApplications(id:Number) = emptyList<ApplicationDTO>()


    //fun createStudent(name: String, email: String, idInst: Number) = students.putIfAbsent(countID, StudentDTO(countID,name,email,idInst, STUDENT_TYPE))

    fun deleteStudent(id:Number) = students.remove(id)
}