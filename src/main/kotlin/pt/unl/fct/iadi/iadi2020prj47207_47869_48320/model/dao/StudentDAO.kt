package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.StudentDTO
import javax.persistence.*


@Entity
@Table(name = "student")
data class StudentDAO(
        override val name:String,
        override val email:String,
        @ManyToOne @JoinColumn(name = "fk_inst",nullable = false, updatable = true)
                      override var inst: InstitutionDAO,
        override val address:String,
        override val phoneNumber:String,
        @OneToMany(mappedBy = "student", cascade = [CascadeType.ALL], orphanRemoval = true)
                      val applications: MutableSet<ApplicationDAO> = mutableSetOf(),
        override val type:String=StudentDTO.STUDENT_TYPE): UserDAO(type,name,email,address,inst,phoneNumber)
