package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao

import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name="user")
abstract class UserDAO(open val type:String,
                       open val name:String,
                       open val email:String,
                       open val address:String,
                       @ManyToOne @JoinColumn(name="fk_inst", nullable = false)
                       open var inst: InstitutionDAO,
                       open val phoneNumber:String){
    @Id @GeneratedValue(strategy = GenerationType.AUTO) open val id:Long=0
}
