package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import java.util.*
import javax.persistence.*

@Entity
@Table(name="evaluation")
data class EvaluationDAO(@Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id:Long = 0,
                         @ManyToOne @JoinColumn(name = "fk_rev",nullable = false)
                         val reviewer: ReviewerDAO,
                         @ManyToOne @JoinColumn(name = "fk_app",nullable = false)
                         val idGA: ApplicationDAO,
                         val points:Int,
                         val notes:String,
                         val date:Date){}
