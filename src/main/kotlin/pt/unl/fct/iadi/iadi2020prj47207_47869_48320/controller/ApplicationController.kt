package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.controller

import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Application
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.SubmissionItemDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.ApplicationService
import java.util.*
import javax.websocket.server.PathParam

@RestController
class ApplicationController(val applicationService:ApplicationService) : Application {
    override fun getAllApplications(pageNumber: Int, pageSize: Int, sorting: List<String>?): Page<ApplicationDTO> =
            applicationService.getAllApplications(pageNumber,pageSize,sorting)

    override fun getApplication(id: Long): ApplicationDTO = applicationService.getApplication(id)

    override fun deleteApplication(id: Long) = applicationService.deleteApplication(id)
}