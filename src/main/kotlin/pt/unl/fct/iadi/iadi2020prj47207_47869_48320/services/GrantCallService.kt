package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.AlreadyExistsException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.InvalidDateFormatException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.InvalidURLException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.GrantCallDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.RequestItemDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.*
import java.sql.Date

@Service
class GrantCallService(val grantRepo:GrantCallRepository,
                       val panelRepo:EvaluationPanelRepository,
                       val reqRepo:RequestItemRepository) {

    fun getAllGrantCalls(status:String?, pageNumber: Int, pageSize: Int,
                         sorting: List<String>?):Page<GrantCallDTO> {
        var res: Page<GrantCallDAO>
        val pageable: Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)

        if (status == null || status == "ALL")
            res = grantRepo.findAll(pageable)
        else if (status == "OPEN")
            res = grantRepo.findAllOpenGrantCalls(pageable)
        else if (status == "CLOSED")
            res = grantRepo.findAllClosedGrantCalls(pageable)
        else
            throw InvalidURLException("Invalid Status param","Request parameter 'status' with value '$status' is invalid. Try instead ALL/OPEN/CLOSED/null")
        return res.map { GrantCallDTO(it) }
    }

    fun getEvalPanelCall(id:Long): EvaluationPanelDTO? {
        val grantapp = grantRepo.findById(id).orElseThrow{
           NotFoundException("No GrantCall Found","No GrantCall with specified id '$id' was found.")
        }
        return grantapp.panel?.let { EvaluationPanelDTO(it) }
    }

    fun getGrantCall(id: Long): GrantCallDTO {
        val dao = grantRepo.findById(id).orElseThrow {
            NotFoundException("No GrantCall Found","No GrantCall with specified id '$id' was found.")
        }
        return GrantCallDTO(dao)
    }

    @Transactional
    fun deleteGrantCall(id: Long) {
        if (grantRepo.existsById(id))
            grantRepo.deleteById(id)
        else
            throw NotFoundException("No GrantCall Found","No GrantCall with specified id '$id' was found.")
    }


    @Transactional
    fun deleteRequestItem(id: Long, item_id: Long) {
        val gc = grantRepo.findById(id).orElseThrow {
            NotFoundException("No GrantCall Found","No GrantCall with specified id '$id' was found.")
        }
        val removed = gc.requestItems.removeIf { it.id == item_id }
        if (removed)
            grantRepo.save(gc)
        else
            throw NotFoundException("No RequestItem Found","No RequestItem with specified id '$item_id' was found.")
    }



    @Transactional
    fun addRequestItem(id: Long, ri:RequestItemDTO):RequestItemDTO {
        val gc = grantRepo.findById(id).orElseThrow {
            NotFoundException("No GrantCall Found","No GrantCall with specified id '$id' was found.")
        }

        val ria = RequestItemDAO(ri.item_name,ri.type,ri.required,gc)
        val exists = gc.requestItems.firstOrNull{ it.item_name == ria.item_name }

        val curr: RequestItemDAO?
        if (exists == null)
            curr = reqRepo.save(ria)
        else
            throw AlreadyExistsException("Request item already exists","The request item with item name ${exists.item_name} already exists.")
        return RequestItemDTO(curr)
    }

    fun getRequestItems(id: Long): List<RequestItemDTO> {
        return grantRepo.findById(id).orElseThrow{
            NotFoundException("No GrantCall Found","No GrantCall with specified id '$id' was found.")
        }.requestItems.map { RequestItemDTO(it) }
    }

    fun getAllItems(): List<RequestItemDTO> {
       return reqRepo.findAll().map { RequestItemDTO(it) }
    }

    @Transactional
    fun updateGrantCall(id: Long, gtCall: GrantCallDTO) {
        val gc = grantRepo.findById(id).orElseThrow {
            NotFoundException("No GrantCall Found","No GrantCall with specified id '$id' was found.")
        }
        gc.title = gtCall.title.ifBlank { gc.title }
        gc.description = gtCall.description.ifBlank { gc.description }
        gc.funding = gtCall.funding
        try {
            gc.open_date = Date.valueOf(gtCall.open_date)
            gc.deadline = Date.valueOf(gtCall.deadline)

        }catch(ex:IllegalArgumentException){
            throw InvalidDateFormatException("Invalid Date Format","${ex.message}")
        }
        grantRepo.save(gc)
    }

    @Transactional
    fun updateRequestItem(id: Long, id2: Long, reqItem: RequestItemDTO) {
        //TODO: Need testing
        val gc = grantRepo.findById(id).orElseThrow {
            NotFoundException("No GrantCall Found","No GrantCall with specified id '$id' was found.")
        }
        val req = gc.requestItems.firstOrNull { it.id == id2 }

        if (req != null){
            req.item_name = reqItem.item_name.ifBlank { req.item_name }
            req.dataType = reqItem.type.ifBlank { req.dataType }
            req.required = reqItem.required
            grantRepo.save(gc)
        }
        else
            throw NotFoundException("No RequestItem Found","No RequestItem with specified id '$id2' was found.")
    }

    fun getGrantCallApplications(id: Long): List<ApplicationDTO> {
        return grantRepo.getApplicationsByGrantCallId(id).map { ApplicationDTO(it.app_id,it.submissionDate,
                it.status.name,it.student.id,it.grant_call.id,
                it.submissionItems.map {
                    it2 -> SubmissionItemDTO(it2.id,it2.submissionItemValue,it2.itemDataType, it2.app!!.app_id)
                })
        }
    }
}