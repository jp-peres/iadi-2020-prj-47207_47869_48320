package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Application.Companion.APPLICATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Evaluation.Companion.EVALUATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.EvaluationPanel.Companion.EVALUATION_PANEL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.GrantCall.Companion.GRANTCALL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Reviewer.Companion.REVIEWER_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.FORBIDDEN
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESPONSE_NOT_FOUND
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.UNAUTHORIZED
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.SERVER_ERROR

@Api(value=EVALUATION_PATH, description = "Evaluation API", tags=["Evaluation"])
@RequestMapping("$RESOURCE/$EVALUATION_PATH")
interface Evaluation{

    companion object{
        const val EVALUATION_PATH = "evaluations"
    }

    @ApiOperation("Requests all evaluations of an application")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Successfully retrieves the list"),
        ApiResponse(code=401, message = UNAUTHORIZED),
        ApiResponse(code=403, message = FORBIDDEN),
        ApiResponse(code=500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.ACCEPTED)
    @GetMapping("{id}")
    fun getEvaluationsOfApplication(@PathVariable id:Long,
                                    @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                                    @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                                    @RequestParam(required=false) sorting:List<String>?):Page<EvaluationDTO>

    @ApiOperation("Request an evaluation made by a certain reviewer")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Successfully obtains the requested evaluation"),
        ApiResponse(code=401, message = UNAUTHORIZED),
        ApiResponse(code=403, message = FORBIDDEN),
        ApiResponse(code=500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.ACCEPTED)
    @GetMapping("$APPLICATION_PATH/{idGA}/$REVIEWER_PATH/{idReviewer}")
    fun getEvaluationOfAppFromReviewer(@PathVariable idReviewer: Long, @PathVariable idGA:Long): EvaluationDTO?

    @ApiOperation("Gets all evaluations made by an evaluation panel")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Successfully obtains a list of an evaluation panel's evaluations"),
        ApiResponse(code=401, message = UNAUTHORIZED),
        ApiResponse(code=403, message = FORBIDDEN),
        ApiResponse(code=500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.ACCEPTED)
    @GetMapping("$EVALUATION_PANEL_PATH/{idPanel}")
    fun getEvaluationFromEvalPanel(@PathVariable idPanel:Long):Collection<EvaluationDTO>

    @ApiOperation("Retrieves all evaluation done to applications for a requested grant call")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Successfully gets a list of the pretended evaluations"),
        ApiResponse(code=401, message = UNAUTHORIZED),
        ApiResponse(code=403, message = FORBIDDEN),
        ApiResponse(code=500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.ACCEPTED)
    @GetMapping("$GRANTCALL_PATH/{idGC}")
    fun getEvaluationsFromCall(@PathVariable idGC:Long):Collection<EvaluationDTO>

    @ApiOperation("Deletes an evaluation with the assigned id")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Evaluation removed"),
        ApiResponse(code=401, message = UNAUTHORIZED),
        ApiResponse(code=404, message = RESPONSE_NOT_FOUND),
        ApiResponse(code=403, message = FORBIDDEN),
        ApiResponse(code=500, message = SERVER_ERROR)
    ])
    @DeleteMapping("$GRANTCALL_PATH/{id}")
    fun deleteEvaluation(id:Long)
}
