package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.*
import java.util.*

@Repository
interface EvaluationPanelRepository: JpaRepository<EvaluationPanelDAO, Long> {

    @Query(value="select reviewers from EvaluationPanelDAO p join p.reviewers reviewers where p.id =:id")
    fun getReviewersFromPanel(@Param(value = "id") id:Long, pageable: Pageable): Page<ReviewerDAO>

    @Query(value="SELECT CASE WHEN COUNT(revs) = 1 THEN true ELSE false END from EvaluationPanelDAO e join e.reviewers revs where revs.id =: revId and e.id =: panelId")
    fun reviewerBelongsToPanel(@Param(value="revId") revId:Long, @Param(value="panelId") panelId:Long):Boolean

    @Query(value="select evaluation from EvaluationPanelDAO p join p.reviewers reviewer join reviewer.evaluations evaluation where p.id =:id")
    fun getEvaluationsFromPanel(@Param(value = "id") id:Long, pageable: Pageable): Page<EvaluationDAO>

    @Query(value = "select p.idChairman from EvaluationPanelDAO p where p.id =:id")
    fun getChairmanOfPanel(@Param(value="id") id:Long): ReviewerDAO?

    @Query(value="select app from EvaluationPanelDAO p join p.grant grant join grant.applications app where p.id =:id and app.app_id =:idGA")
    fun getApplicationFromPanel(@Param(value = "id") id:Long, @Param(value="idGA") idGA:Long): Optional<ApplicationDAO>

    @Query(value="select apps from EvaluationPanelDAO p join p.grant grant join grant.applications apps where p.id =:id")
    fun getApplicationsFromPanel(@Param(value = "id") id:Long, pageable: Pageable): Page<ApplicationDAO>
}