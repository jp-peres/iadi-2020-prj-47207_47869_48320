package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import java.util.*


class JwtUtils {
    companion object{
        private const val SECRET = "anBlcmVzamdjZHRiaWNhSUFESTIwMjA="
        private const val TTL = 1800000  // 30 mins

        fun createJwt(auth:Authentication):String {
            val claims:MutableMap<String,Any> = mutableMapOf()
            claims["authorities"] = auth.authorities
            return Jwts.builder()
                    .setClaims(claims)
                    .setSubject(auth.name)
                    .setIssuedAt(Date(System.currentTimeMillis()))
                    .setExpiration(Date(System.currentTimeMillis() + TTL))
                    .signWith(SignatureAlgorithm.HS256,SECRET)
                    .compact()
        }

        fun mockJwt(user:String,role:String):String {
            val claims:MutableMap<String,Any> = mutableMapOf()
            claims["authorities"] = listOf(SimpleGrantedAuthority(role))
            val token = Jwts.builder()
                    .setClaims(claims)
                    .setSubject(user)
                    .setIssuedAt(Date(System.currentTimeMillis()))
                    .setExpiration(Date(System.currentTimeMillis() + TTL))
                    .signWith(SignatureAlgorithm.HS256,SECRET)
                    .compact()
            return "Bearer $token"
        }

        fun extractClaims(jwt:String):Claims {
            return Jwts.parser().setSigningKey(SECRET).parseClaimsJws(jwt).body
        }
    }
}