package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig(val userDetailsService: GrantUserDetailsService) : WebSecurityConfigurerAdapter() {

    @Bean
    fun getPassEncoder():PasswordEncoder = BCryptPasswordEncoder()


    override fun configure(http: HttpSecurity) {
        http.cors().and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(JwtUserPasswordAuthFilter(authenticationManager()))
                .addFilterAfter(JwtValidationFilter(), JwtUserPasswordAuthFilter::class.java)
                .authorizeRequests()
                .antMatchers("/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/configuration/security",
                        "/swagger-ui.html",
                        "/webjars/**", "/login", "/logout").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .logout()
                    .clearAuthentication(true)
                    .invalidateHttpSession(true)
                    .deleteCookies("JSESSIONID")
                    .logoutSuccessUrl("/swagger-ui.html")

    }


//    .antMatchers("/$RESOURCE/$STUDENT_PATH/**","/$RESOURCE/$APPLICATION_PATH/**").hasAnyRole(STUDENT.name,ADMIN.name)
//    .antMatchers("/$RESOURCE/$REVIEWER_PATH/**"
//    ,"/$RESOURCE/$EVALUATION_PATH/**"
//    ,"/$RESOURCE/$EVALUATION_PANEL_PATH/**").hasAnyRole(REVIEWER.name,ADMIN.name)
//    .antMatchers("/$RESOURCE/$GRANTCALL_PATH/**"
//    ,"/$RESOURCE/$EVALUATION_PANEL_PATH/**"
//    ,"/$RESOURCE/$SPONSOR_PATH/**").hasAnyRole(SPONSOR_PATH,ADMIN.name)
//    .antMatchers("/$RESOURCE/**").hasRole(ADMIN.name)


    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(daoProvider())
    }

    @Bean
    fun daoProvider(): DaoAuthenticationProvider{
        val provider = DaoAuthenticationProvider()
        provider.setPasswordEncoder(getPassEncoder())
        provider.setUserDetailsService(userDetailsService)
        return provider
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource? {
        val configuration = CorsConfiguration()
        configuration.allowedOrigins = listOf("*")
        configuration.allowedMethods = listOf("GET","POST","PUT","DELETE")
        configuration.allowedHeaders = listOf("Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization")
        configuration.exposedHeaders = listOf("Authorization")
        configuration.addAllowedHeader("Access-Control-Allow-Origin")
        configuration.addAllowedHeader("Access-Control-Allow-Headers")
        configuration.addAllowedHeader("Access-Control-Allow-Methods")
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }
}