package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.*
import java.security.Principal


@Service("secureServ")
class SecurityService(val appRepo: ApplicationRepository,
                      val evalPanelRepo: EvaluationPanelRepository,
                      val grantRepo: GrantCallRepository,
                      val evalRepo: EvaluationRepository,
                      val instRepo: InstitutionRepository,
                      val studRepo: StudentRepository,
                      val revRepo: ReviewerRepository,
                      val spoRepo: SponsorRepository) {

    fun principalIsStudent(principal: String, id:Long):Boolean {
        return studRepo.findById(id).orElseThrow {
            NotFoundException("Student not found","Student with id $id was not found.")
        }.name == principal
    }

    fun principalIsReviewer(name:String,id:Long):Boolean {
        return revRepo.findById(id).orElseThrow {
            NotFoundException("Reviewer not found","Reviewer with id $id was not found.")
        }.name == name
    }

    fun principalIsSponsor(name:String,id:Long):Boolean {
        return spoRepo.findById(id).orElseThrow {
            NotFoundException("Sponsor not found","Sponsor with id $id was not found.")
        }.name == name
    }

    fun relationshipIsSame(id:Long, dtoId:Long):Boolean {
        return id == dtoId;
    }

    @Deprecated("A work around since before I wasnt being able to access the current user")
    fun getCurrentPrincipal():Authentication {
        return SecurityContextHolder.getContext().authentication
    }
}