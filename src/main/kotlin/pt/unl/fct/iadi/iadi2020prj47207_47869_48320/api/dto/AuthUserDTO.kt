package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto

data class AuthUserDTO(val username:String,
                  val password:String,
                  val active:Boolean,
                  val roles:String)