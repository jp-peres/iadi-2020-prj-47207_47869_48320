package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtUserPasswordAuthFilter(private val authManager: AuthenticationManager) : UsernamePasswordAuthenticationFilter() {

    override fun attemptAuthentication(request: HttpServletRequest?, response: HttpServletResponse?): Authentication {

        val obj = ObjectMapper().readValue(request!!.inputStream,AuthRequest::class.java)

        val auth = UsernamePasswordAuthenticationToken(obj.getUsername(),obj.getPassword())

        val res = authManager.authenticate(auth)
        return res
    }

    override fun successfulAuthentication(request: HttpServletRequest?, response: HttpServletResponse?,
                                          chain: FilterChain?, authResult: Authentication?) {
        val jwtToken = JwtUtils.createJwt(authResult!!)
        response!!.addHeader("Authorization", "Bearer $jwtToken")
    }
}