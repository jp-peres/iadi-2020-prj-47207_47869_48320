package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.InstitutionDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.InstitutionRepository

@Service
class InstitutionService(val inst_repo: InstitutionRepository) {

    fun getAllInstitutions(pageNumber:Int, pageSize:Int, sorting:List<String>?):Page<InstitutionDTO> {
        val pageable:Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        val page = inst_repo.findAll(pageable)
        return page.map { InstitutionDTO(it) }
    }

    @Transactional
    fun createInstitution(inst:InstitutionDTO): InstitutionDTO {
        val dao = inst_repo.save(InstitutionDAO(inst.name,inst.nipc,inst.location,inst.contact))
        return InstitutionDTO(dao)
    }

    fun getInstitutionStudents(id:Long, pageNumber:Int, pageSize:Int,
                               sorting:List<String>?):Page<StudentDTO> {

        inst_repo.findById(id).orElseThrow{
            NotFoundException("No Institution Found","No institution with specified id '$id' was found.")
        }
        val pageable:Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return inst_repo.getInstitutionStudentsFromId(id,pageable).map{ StudentDTO(it) }
    }

    fun getInstitutionReviewers(id:Long,pageNumber:Int, pageSize:Int,
                                sorting:List<String>?):Page<ReviewerDTO> {
        inst_repo.findById(id).orElseThrow{
            NotFoundException("No Institution Found","No institution with specified id '$id' was found.")
        }
        val pageable:Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return inst_repo.getInstitutionReviewersById(id,pageable).map { ReviewerDTO(it) }
    }

    @Transactional
    fun deleteInstitution(id:Long) {
        if(inst_repo.existsById(id))
            inst_repo.deleteById(id)
        else
            throw NotFoundException("No Institution Found","No institution with specified id '$id' was found.")
    }

    fun getInstitution(id:Long): InstitutionDTO? {
        val dao = inst_repo.findById(id).orElseThrow {
            NotFoundException("No Institution Found","No institution with specified id '$id' was found.")
        }
        return InstitutionDTO(dao.idInst, dao.name, dao.nipc, dao.location, dao.contact)
    }

    @Transactional
    fun updateInstitution(id: Long, inst: InstitutionDTO) {
        var curr = inst_repo.findById(id).orElseThrow {
            NotFoundException("No Sponsor Found","No Sponsor with specified id '$id' was found.")
        }

        if (inst.name != null)
            curr.name = inst.name
        if (inst.contact!= null)
            curr.contact = inst.contact
        if (inst.location!=null)
            curr.location = inst.location
        if (inst.nipc!=null)
            curr.nipc = inst.nipc

        inst_repo.save(curr)
    }
}