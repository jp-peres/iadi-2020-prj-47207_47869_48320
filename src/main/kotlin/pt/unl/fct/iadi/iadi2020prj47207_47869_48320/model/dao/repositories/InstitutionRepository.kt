package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.InstitutionDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ReviewerDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.StudentDAO

@Repository
interface InstitutionRepository: JpaRepository<InstitutionDAO, Long> {
    @Query(value = "select users from InstitutionDAO i join i.users users where i.idInst =:id and users.type ='STUDENT'")
    fun getInstitutionStudentsFromId(@Param(value = "id") id:Long, pageable: Pageable):Page<StudentDAO>

    @Query(value = "select users from InstitutionDAO i join i.users users where i.idInst =:id and users.type ='REVIEWER'")
    fun getInstitutionReviewersById(@Param(value="id") id:Long, pageable: Pageable):Page<ReviewerDAO>
}