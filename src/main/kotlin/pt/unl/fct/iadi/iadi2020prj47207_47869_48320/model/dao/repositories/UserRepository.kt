package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.NoRepositoryBean
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.UserDAO

@NoRepositoryBean
interface UserRepository<T: UserDAO>: JpaRepository<T,Long> {}
