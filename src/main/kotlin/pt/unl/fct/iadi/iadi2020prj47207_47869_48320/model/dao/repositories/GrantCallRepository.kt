package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ApplicationDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.GrantCallDAO

@Repository
interface GrantCallRepository: JpaRepository<GrantCallDAO, Long> {
    @Query("select gc from GrantCallDAO gc where current_date between gc.open_date and gc.deadline")
    fun findAllOpenGrantCalls(pageable: Pageable): Page<GrantCallDAO>

    @Query("select gc from GrantCallDAO gc where current_date > gc.deadline")
    fun findAllClosedGrantCalls(pageable: Pageable): Page<GrantCallDAO>

    @Query(value = "select apps from GrantCallDAO gc join gc.applications apps")
    fun getApplicationsByGrantCallId(id:Long):MutableIterable<ApplicationDAO>
}