package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security

import com.fasterxml.jackson.databind.ObjectMapper
import io.jsonwebtoken.Claims
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.MalformedJwtException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.OncePerRequestFilter
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.LinkedHashMap
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtValidationFilter: OncePerRequestFilter() {
    override fun doFilterInternal(req: HttpServletRequest, resp: HttpServletResponse, chain: FilterChain) {

        val authHeader = req.getHeader("Authorization")

        if (authHeader.isNullOrEmpty() || !authHeader.startsWith("Bearer ")) {
            chain.doFilter(req,resp)
            return
        }
        val token = authHeader.substring(7)

        var claims: Claims? = null
        try {
            claims = JwtUtils.extractClaims(token)
        }catch(ex:JwtException){
            var error = "Jwt Error"
            if (ex is MalformedJwtException)
                error = "Jwt Malformed/Corrupted"
            else if (ex is ExpiredJwtException)
                error = "Jwt Expired"
            buildErrorResponse(error, ex.message.toString(), req, resp)
            return
        }


        val username = claims.subject

        val authorities =  claims.get("authorities") as List<Map<String,String>>

        val grantAuthorities =  authorities.map { a -> SimpleGrantedAuthority(a.get("authority")) }

        val authentication = UsernamePasswordAuthenticationToken(username,null,grantAuthorities)

        SecurityContextHolder.getContext().authentication = authentication

        chain.doFilter(req,resp)
    }

    private fun buildErrorResponse(error: String, errorMsg: String, req: HttpServletRequest, resp: HttpServletResponse) {
        val body: MutableMap<String, Any> = LinkedHashMap()
        val objMap: ObjectMapper = ObjectMapper()
        body["timestamp"] = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"))
        body["error"] = error
        body["status"] = HttpStatus.FORBIDDEN.value()
        body["message"] = errorMsg
        body["path"] = req.requestURI
        val response = objMap.writeValueAsString(body)
        resp.contentType = MediaType.APPLICATION_JSON_VALUE
        resp.writer.print(response)
        resp.status = HttpStatus.FORBIDDEN.value()
    }

}