package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.SERVER_ERROR
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.FORBIDDEN
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.UNAUTHORIZED
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Application.Companion.APPLICATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.AllowedForApplicationOperations

@Api(value=APPLICATION_PATH, description = "GrantApplication API", tags=["GrantApplication"])
@RequestMapping("$RESOURCE/$APPLICATION_PATH")
interface Application {
    companion object {
        const val APPLICATION_PATH: String = "applications"
    }

    @ApiOperation(value = "Returns the list of all applications in the system.", nickname="getApplication")
    @ApiResponses(value = [
        ApiResponse(code=200,message = "List of all applications in the system."),
        ApiResponse(code=401,message = UNAUTHORIZED),
        ApiResponse(code=403,message = FORBIDDEN),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForApplicationOperations
    @GetMapping("")
    fun getAllApplications(@RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                           @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                           @RequestParam(required=false) sorting:List<String>?): Page<ApplicationDTO>

    @ApiOperation(value = "Returns the application with respective id.", nickname="getApplication")
    @ApiResponses(value = [
        ApiResponse(code=200,message = "Returns the application with given id"),
        ApiResponse(code=401,message = UNAUTHORIZED),
        ApiResponse(code=403,message = FORBIDDEN),
        ApiResponse(code=404,message = "Application with id was not found"),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForApplicationOperations
    @GetMapping("{id}")
    fun getApplication(@PathVariable id:Long): ApplicationDTO?

    @ApiOperation(value = "Deletes the application that has the specified id.", nickname="deleteApplication")
    @ApiResponses(value = [
        ApiResponse(code=204,message = "The application with id was removed successfully"),
        ApiResponse(code=401,message = UNAUTHORIZED),
        ApiResponse(code=403,message = FORBIDDEN),
        ApiResponse(code=404,message = "The application with id doesnt exist"),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowedForApplicationOperations
    @DeleteMapping("{id}")
    fun deleteApplication(@PathVariable id:Long)
}