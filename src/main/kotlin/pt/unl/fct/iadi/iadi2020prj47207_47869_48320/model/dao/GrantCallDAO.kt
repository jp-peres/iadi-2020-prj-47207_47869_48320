package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao

import java.util.Date
import javax.persistence.*

@Entity
@Table(name="grant_call")
data class GrantCallDAO(var title:String,
                        var description:String,
                        var requirements:String,
                        var funding:Float,
                        var open_date: Date, var deadline: Date,
                        @ManyToOne @JoinColumn(name = "fk_sp",nullable = false)
                        val sponsor:SponsorDAO) {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id:Long = 0
    @OneToOne(mappedBy = "grant", cascade = [CascadeType.ALL], orphanRemoval = true)
    var panel: EvaluationPanelDAO? = null
    @OneToMany(mappedBy = "grant_call", cascade = [CascadeType.ALL], orphanRemoval = true)
    val applications: MutableSet<ApplicationDAO> = mutableSetOf()
    @OneToMany(mappedBy = "grant_call_req",cascade = [CascadeType.ALL],orphanRemoval = true)
    val requestItems: MutableSet<RequestItemDAO> = mutableSetOf()
}
