package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import javax.persistence.*

@Entity
@Table(name = "reviewer")
data class ReviewerDAO(
                       override val name:String,
                       override val email:String,
                       @ManyToOne @JoinColumn(name = "fk_inst",nullable = false)
                       override var inst: InstitutionDAO,
                       override val address:String,
                       override val phoneNumber:String,
                       override val type:String=ReviewerDTO.REVIEWER_TYPE): UserDAO(type,name,email,address,inst,phoneNumber){
    @OneToMany(mappedBy = "reviewer",cascade = [CascadeType.ALL], orphanRemoval = true)
    val evaluations:Set<EvaluationDAO> = mutableSetOf()
    @ManyToMany(mappedBy = "reviewers")
    val panels:Set<EvaluationPanelDAO> = mutableSetOf()
}
