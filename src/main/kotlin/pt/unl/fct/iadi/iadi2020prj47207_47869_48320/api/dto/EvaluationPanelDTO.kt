package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.EvaluationPanelDAO

data class EvaluationPanelDTO(val id:Long, val idCall:Long, val idChairman:Long, val academicArea:String) {
    constructor(it:EvaluationPanelDAO):this(it.id, it.grant.id, it.idChairman.id,it.academicArea)
}