package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services

import org.springframework.stereotype.Service
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO

@Deprecated("Dead. May be rezzed after change in approach")
@Service
class ReviewerService {

    companion object{
        const val REVIEWER_TYPE:String = "REVIEWER"
    }

    fun getAllReviewers() = emptyList<ReviewerDTO>()

    //fun getReviewer(id:Number) = ReviewerDTO(id,"Test $id", "FCT NOVA DI", id, REVIEWER_TYPE);

}
