package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.AuthUserDAO
import java.util.*

@Repository
interface AuthUserRepository: JpaRepository<AuthUserDAO,Long>{
    fun findByUsername(username:String):Optional<AuthUserDAO>
    fun existsByUsername(username:String):Boolean
    fun deleteByUsername(username: String)
}