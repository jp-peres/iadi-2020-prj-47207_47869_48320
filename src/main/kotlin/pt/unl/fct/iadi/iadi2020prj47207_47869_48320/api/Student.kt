package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Application.Companion.APPLICATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.FORBIDDEN
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.StudentDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Evaluation.Companion.EVALUATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Student.Companion.STUDENT_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.GrantCall.Companion.GRANTCALL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.SERVER_ERROR
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.UNAUTHORIZED
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.GrantCallDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.*

@Api(value="students", description = "Student API", tags=["Student"])
@RequestMapping("$RESOURCE/$STUDENT_PATH")
interface Student {

    companion object {
        const val STUDENT_PATH: String = "students"
    }

    @ApiOperation(value = "Retrieves all students from the system", nickname="getAllStudents")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns all students"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetAllStudents
    @GetMapping("")
    fun getAllStudents(@RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                       @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                       @RequestParam(required=false) sorting:List<String>?):Page<StudentDTO>


    @ApiOperation(value = "Retrieves a student with given id", nickname="getStudent")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns the student with given id"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "Student with id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetStudent
    @GetMapping("{id}")
    fun getStudent(@PathVariable id:Long): StudentDTO?


    @ApiOperation(value = "Deletes a student with given id", nickname="removeStudent")
    @ApiResponses(value = [
        ApiResponse(code = 204,message = "Deleted the student successfully"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404,message = "Student with id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowedForDeletionStudent
    @DeleteMapping("{id}")
    fun removeStudent(@PathVariable id:Long)

    @ApiOperation(value = "Retrieves the grant applications from a student with given id", nickname="getStudentApplications")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns the grant applications made by the student"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "Student with id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetStudentApplications
    @GetMapping("{id}/$APPLICATION_PATH")
    fun getStudentApplications(@PathVariable id:Long,
                               @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                               @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                               @RequestParam(required=false) sorting:List<String>?): Page<ApplicationDTO>

    @ApiOperation(value = "Retrieves a grant application with id2 belonging to the student with id", nickname="getStudentApplication")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns the grant application with specified id2"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "Student/Application with id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetStudentApplication
    @GetMapping("{id}/$APPLICATION_PATH/{id2}")
    fun getStudentApplication(@PathVariable id:Long,@PathVariable id2:Long): ApplicationDTO

    @ApiOperation(value = "Retrieves the evaluations done to a student's application", nickname="getReviewsOfApplication")
    @ApiResponses(value = [
        ApiResponse(code=200,message = "Retrieves the reviews of the application with specified id"),
        ApiResponse(code=401,message = UNAUTHORIZED),
        ApiResponse(code=403,message = FORBIDDEN),
        ApiResponse(code=404,message = "The Student/Application with id doesnt exist"),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetStudentApplicationReviews
    @GetMapping("{id}/$APPLICATION_PATH/{id2}/$EVALUATION_PATH")
    fun getReviewsOfApplicationOfStudent(@PathVariable id:Long, @PathVariable id2:Long,
                                         @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                                         @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                                         @RequestParam(required=false) sorting:List<String>?): Page<EvaluationDTO>

    @ApiOperation(value = "Retrieves the status of the grant application with id2 belonging to the student with id", nickname="getStudentApplicationStatus")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns the status of the grant application with specified id2"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "Student/Application with id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetStudentApplicationStatus
    @GetMapping("{id}/$APPLICATION_PATH/{id2}/status")
    fun getStudentApplicationStatus(@PathVariable id:Long,@PathVariable id2:Long): String

    @ApiOperation(value = "Creates a new application", nickname="createApplication")
    @ApiResponses(value = [
        ApiResponse(code = 201, message = "The application was created successfully"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.CREATED)
    @AllowedForCreationApplication
    @PostMapping("{id}/$APPLICATION_PATH",consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun createApplication(@PathVariable id: Long, @RequestBody app:ApplicationDTO): ApplicationDTO?

    @ApiOperation(value = "Retrieves list of all open grant calls")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved list of opened grant calls"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetOpenGrantCalls
    @GetMapping(GRANTCALL_PATH)
    fun getOpenGrantCalls(@RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                          @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                          @RequestParam(required=false) sorting:List<String>?):Page<GrantCallDTO>
}