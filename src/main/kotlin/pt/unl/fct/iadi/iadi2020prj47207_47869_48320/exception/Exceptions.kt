package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception

import java.lang.RuntimeException


open class MyException(open val error:String, override var message: String):RuntimeException(message)
class NotFoundException(override val error:String, override var message:String):MyException(error,message)
class InvalidURLException(override val error:String, override var message:String):MyException(error,message)
class AlreadyExistsException(override val error:String, override var message:String):MyException(error,message)
class InvalidDateFormatException(override val error:String, override var message:String):MyException(error,message)
class ApplicationDecisionException(override val error:String, override var message:String):MyException(error,message)
class InvalidPageParameterException(override val error:String, override var message:String):MyException(error,message)