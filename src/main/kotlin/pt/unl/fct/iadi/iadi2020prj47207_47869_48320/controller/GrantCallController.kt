package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.controller

import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.GrantCall
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationPanelDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.GrantCallDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.RequestItemDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.GrantCallService

@RestController
class GrantCallController(val gCall: GrantCallService):GrantCall{

    override fun getAllGrantCalls(status: String?, pageNumber: Int, pageSize: Int,
                                  sorting: List<String>?): Page<GrantCallDTO> =
            gCall.getAllGrantCalls(status,pageNumber,pageSize,sorting)

    override fun getOpenGrantCalls(pageNumber: Int, pageSize: Int,
                                   sorting: List<String>?): Page<GrantCallDTO> =
            gCall.getAllGrantCalls("OPEN",pageNumber,pageSize,sorting)

    override fun getEvalPanelGrantCall(id: Long): EvaluationPanelDTO? = gCall.getEvalPanelCall(id)

    override fun getGrantCall(id: Long): GrantCallDTO? = gCall.getGrantCall(id)

    override fun updateGrantCall(id: Long, grantCall: GrantCallDTO) = gCall.updateGrantCall(id,grantCall)

    override fun deleteGrantCall(id: Long) = gCall.deleteGrantCall(id)

    override fun deleteRequestItem(id: Long, item_id: Long) = gCall.deleteRequestItem(id,item_id)

    override fun addRequestItem(id: Long, reqItem: RequestItemDTO): RequestItemDTO? = gCall.addRequestItem(id, reqItem)

    override fun updateRequestItem(id: Long, id2: Long, reqItem: RequestItemDTO) = gCall.updateRequestItem(id,id2,reqItem)

    override fun getRequestItems(id: Long): List<RequestItemDTO>? = gCall.getRequestItems(id)

    override fun getAllItems():List<RequestItemDTO> = gCall.getAllItems()

    override fun getGrantCallApplications(id: Long): List<ApplicationDTO> = gCall.getGrantCallApplications(id)

}