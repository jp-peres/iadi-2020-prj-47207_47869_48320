package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.InstitutionDTO
import javax.persistence.*

@Entity
@Table(name = "institution")
data class InstitutionDAO(var name:String,
                          var nipc:Long,
                          var location:String,
                          var contact:String) {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val idInst:Long = 0
    @OneToMany(mappedBy = "inst",cascade = [CascadeType.ALL], orphanRemoval = true)
    val users:MutableSet<UserDAO> = mutableSetOf()
}