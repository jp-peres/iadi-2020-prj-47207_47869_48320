package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao

import javax.persistence.*

@Entity
@Table(name="request_item")
data class RequestItemDAO(var item_name:String,
                          var dataType:String,
                          var required:Boolean,
                          @ManyToOne @JoinColumn(name="call_id", nullable=false)
                          val grant_call_req: GrantCallDAO){
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id:Long = 0
}