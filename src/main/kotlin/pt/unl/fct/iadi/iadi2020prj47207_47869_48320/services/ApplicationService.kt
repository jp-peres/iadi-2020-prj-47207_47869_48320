package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services

import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ApplicationDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.ApplicationRepository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.GrantCallRepository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.StudentRepository

@Service
class ApplicationService(val appRepo:ApplicationRepository,
                         val studRepo:StudentRepository,
                         val callRepo:GrantCallRepository){

    fun getAllApplications(pageNumber: Int, pageSize: Int, sorting: List<String>?):Page<ApplicationDTO> {
        val pageable = PageableBuilder.build(0,15,sorting)
        return appRepo.findAll(pageable).map {
            ApplicationDTO(it)
        }
    }

    fun getApplication(id:Long):ApplicationDTO {
        val app = appRepo.findById(id).orElseThrow {
            NotFoundException("No Application was found","No application with specified id '$id' was found.")
        }
        return ApplicationDTO(app)
    }

    @Transactional
    fun deleteApplication(id: Long) {
        if(appRepo.existsById(id))
            appRepo.deleteById(id)
        else
            throw NotFoundException("No Application Found","No application with specified id '$id' was found.")
    }
}