package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.EvaluationDAO
import java.util.*

@Repository
interface EvaluationRepository:JpaRepository<EvaluationDAO,Long> {

    @Query(value = "select eval from EvaluationDAO eval where eval.idGA.app_id=:idGA")
    fun findByGrantApplication(@Param(value = "idGA")idGA:Long, pageable: Pageable):Page<EvaluationDAO>

    @Query(value = "select eval from EvaluationDAO eval where eval.idGA.app_id=:idGA and eval.reviewer.id=:idReviewer")
    fun findByGrantApplicationAndReviewer(@Param(value = "idReviewer")idReviewer:Long,@Param(value = "idGA")idGA:Long):Optional<EvaluationDAO>

    @Query(value = "select eval from GrantCallDAO grant join ApplicationDAO join EvaluationDAO eval where grant.id=:idGC")
    fun findByGrantCall(@Param(value = "idGC")idGC:Long):List<EvaluationDAO>

}