package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationPanelDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.GrantCallDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.SponsorDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.AlreadyExistsException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.InvalidDateFormatException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.GrantRoles.SPONSOR
import java.sql.Date

@Service
class SponsorService(val sponsorRepo:SponsorRepository, val authRepository: AuthUserRepository,
                     val reqRepo:RequestItemRepository, val revRepo:ReviewerRepository,
                     val grantRepo:GrantCallRepository, val panelRepo:EvaluationPanelRepository) {

    fun getSponsors(pageNumber: Int, pageSize: Int, sorting: List<String>?): Page<SponsorDTO> {
        val pageable: Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return sponsorRepo.findAll(pageable).map {
                SponsorDTO(it.id,it.name,it.email,it.contact,it.grantCalls.map {
                    it2 -> GrantCallDTO(it2)
            })
        }
    }

    fun getSponsor(id: Long): SponsorDTO? {
        val sponsor = sponsorRepo.findById(id).orElseThrow {
            NotFoundException("No Sponsor Found","No sponsor with specified id '$id' was found.")
        }
        return SponsorDTO(sponsor)
    }

    fun getSponsorGrantCalls(id: Long, pageNumber: Int, pageSize: Int, sorting: List<String>?): Page<GrantCallDTO>{
        if(!sponsorRepo.existsById(id))
           throw NotFoundException("No Sponsor Found","No sponsor with specified id '$id' was found.")
        val pageable: Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return sponsorRepo.getGrantCallsBySponsorId(id,pageable).map { GrantCallDTO(it) }
    }

    @Transactional
    fun createSponsor(sponsor:SponsorDTO,password:String): SponsorDTO? {
        if (!authRepository.existsByUsername(sponsor.name))
            authRepository.save(AuthUserDAO(sponsor.name,password,true, SPONSOR.name))
        else
            throw AlreadyExistsException("User Already Exists", "User with name '${sponsor.name}' already exists.")

        val created = sponsorRepo.save(SponsorDAO(sponsor.name,sponsor.email,sponsor.contact))
        return SponsorDTO(created)
    }

    @Transactional
    fun deleteSponsor(id: Long) {
        val curr = sponsorRepo.findById(id).orElseThrow {
            throw NotFoundException("No Sponsor Found", "No Sponsor with specified id '$id' was found.")
        }
        sponsorRepo.deleteById(id)
        authRepository.deleteByUsername(curr.name)
    }

    @Transactional
    fun updateSponsor(id: Long, sponsor: SponsorDTO) {
        var curr = sponsorRepo.findById(id).orElseThrow {
            NotFoundException("No Sponsor Found","No Sponsor with specified id '$id' was found.")
        }
        if (sponsor.email != null)
            curr.email = sponsor.email
        if (sponsor.contact!= null)
            curr.contact = sponsor.contact
        if (sponsor.name!=null)
            curr.name = sponsor.name

        sponsorRepo.save(curr);
    }

    @Transactional
    fun createGrantCall(id: Long, gc: GrantCallDTO,
                        ep: EvaluationPanelDTO, revs: List<Long>): GrantCallDTO? {
        val sponsor = sponsorRepo.findById(id).orElseThrow{
            NotFoundException("No Sponsor Found","No Sponsor with specified id '$id' was found.")
        }

        if(sponsor.grantCalls.find { it.title == gc.title } != null)
            throw AlreadyExistsException("GrantCall already Exists","A GrantCall with title '${gc.title}' already exists")

        var createdGC : GrantCallDAO

        try {
            //TODO: Deal with ISO Formatting
            val st = Date.valueOf(gc.open_date)
            val end = Date.valueOf(gc.open_date)
            createdGC = GrantCallDAO(gc.title,gc.description,gc.requirements,gc.funding,st,end,sponsor)
        }catch(ex:IllegalArgumentException){
            throw InvalidDateFormatException("Invalid Date Format","${ex.message}")
        }

        sponsor.grantCalls.add(createdGC)

        val reviewers = revRepo.findAllById(revs)
        if (reviewers.size != revs.size)
            throw NotFoundException("Reviewers not found","Not found reviewer(s) from specified id list '$revs'")
        val chairman = reviewers.firstOrNull { it.id == ep.idChairman }
                ?: throw NotFoundException("Chairman not found", "Chairman wasnt found in specified list '$revs'")

        val evalPanel = EvaluationPanelDAO(createdGC,chairman,ep.academicArea,reviewers)

        createdGC.panel = evalPanel

        val reqItemDAOs: MutableSet<RequestItemDAO> = buildReqItems(gc, createdGC)

        createdGC.requestItems.addAll(reqItemDAOs)

        sponsorRepo.save(sponsor)

        val finalGC = grantRepo.findById(createdGC.id).get()

        return GrantCallDTO(finalGC)
    }

    private fun buildReqItems(gc: GrantCallDTO, createdGC: GrantCallDAO): MutableSet<RequestItemDAO> {
        val reqNames = gc.reqItems.map { it.item_name }
        val reqItemDAOs = mutableSetOf<RequestItemDAO>()

        val existing = reqRepo.findAllByItem_name(reqNames)
        if (existing.size != 0) {
            reqItemDAOs.addAll(existing)

            val newItems = reqItemDAOs.flatMap { reqDAO -> gc.reqItems.filter { reqDAO.item_name != it.item_name } }

            reqItemDAOs.addAll(
                    newItems.map { RequestItemDAO(it.item_name, it.type, it.required, createdGC) }
            )
        } else {
            reqItemDAOs.addAll(gc.reqItems.map { RequestItemDAO(it.item_name, it.type, it.required, createdGC) })
        }
        return reqItemDAOs
    }
}