package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.GrantCallDAO
import java.time.LocalDate
import java.util.*

data class GrantCallDTO(val id:Long, val id_sponsor:Long, val id_panel:Long?, val title:String, val description:String, val requirements:String,
                        val funding:Float, val open_date:String, val deadline:String, val reqItems:List<RequestItemDTO>) {
    constructor(gc:GrantCallDAO):this(gc.id, gc.sponsor.id, gc.panel?.id,
            gc.title, gc.description, gc.requirements, gc.funding, gc.open_date.toString(),
            gc.deadline.toString(), gc.requestItems.map { RequestItemDTO(it) })
}