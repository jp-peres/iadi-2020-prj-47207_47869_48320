package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto

interface UserDTO{
    val id:Long
    val type:String
    val name:String
    val email:String
    val idInst:Long?
    val address:String
    val phoneNumber:String
}
