package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ApplicationDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.StudentDAO
import java.util.*

@Repository
interface StudentRepository:UserRepository<StudentDAO> {
    @Query(value="select apps from StudentDAO s join s.applications apps where s.id = ?1")
    fun getStudentApplications(id:Long,pageable: Pageable):Page<ApplicationDAO>

    @Query(value="select app from StudentDAO s join s.applications app where s.id = ?1 and app.app_id = ?2")
    fun getStudentApplication(id:Long,id2:Long):Optional<ApplicationDAO>
}