package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.InstitutionDAO

data class InstitutionDTO(val id:Long, val name:String,val nipc:Long, val location:String, val contact:String){
    constructor(it: InstitutionDAO):this(it.idInst,it.name,it.nipc,it.location,it.contact)
}

