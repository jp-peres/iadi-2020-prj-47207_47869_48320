package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.SponsorDAO

data class SponsorDTO(val id:Long, val name:String, val email:String, val contact:String, val grantCalls:List<GrantCallDTO>){
    constructor(it:SponsorDAO):this(it.id,it.name,it.email,it.contact,it.grantCalls.map {
        it2 -> GrantCallDTO(it2)
    })
}