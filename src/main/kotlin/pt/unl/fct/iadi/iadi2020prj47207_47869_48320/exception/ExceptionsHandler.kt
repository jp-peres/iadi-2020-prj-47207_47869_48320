package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


@ControllerAdvice
class ControllerExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [NotFoundException::class,AlreadyExistsException::class,
        InvalidURLException::class,InvalidDateFormatException::class,ApplicationDecisionException::class,
        InvalidPageParameterException::class])
    fun handleException(
            ex: MyException, request: WebRequest): ResponseEntity<Any> {

        val body: MutableMap<String, Any> = LinkedHashMap()
        body["timestamp"] = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"))
        body["error"] = ex.error
        body["message"] = ex.message
        body["path"] = request.getDescription(false).removePrefix("uri=")

        var responseCode:HttpStatus
        if (ex is NotFoundException)
            responseCode = HttpStatus.NOT_FOUND
        else if (ex is AlreadyExistsException)
            responseCode = HttpStatus.CONFLICT
        else if (ex is ApplicationDecisionException)
            responseCode = HttpStatus.FORBIDDEN
        else
            responseCode = HttpStatus.BAD_REQUEST

        return handleExceptionInternal(ex, body,
                HttpHeaders(), responseCode, request)
    }
}