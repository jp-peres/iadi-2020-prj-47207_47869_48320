package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.FORBIDDEN
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.SERVER_ERROR
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.UNAUTHORIZED
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Sponsor.Companion.SPONSOR_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.GrantCall.Companion.GRANTCALL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationPanelDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.GrantCallDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.SponsorDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.*

@Api(value="Sponsor", description = "Sponsor API", tags=["Sponsor"])
@RequestMapping("$RESOURCE/$SPONSOR_PATH")
interface Sponsor {
    companion object {
        const val SPONSOR_PATH: String = "sponsors"
    }

    @ApiOperation(value = "Retrieves all the sponsors from the system", nickname="getSponsors")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns all sponsors"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetAllSponsors
    @GetMapping("")
    fun getSponsors(@RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                    @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                    @RequestParam(required=false) sorting:List<String>?):Page<SponsorDTO>


    @ApiOperation(value = "Returns the sponsor with specified id", nickname="getSponsor")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns the sponsor with specified id"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "Sponsor with id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetSponsor
    @GetMapping("{id}")
    fun getSponsor(@PathVariable id:Long): SponsorDTO?


    @ApiOperation(value = "Retrieves all the grant calls from a sponsor with specified id", nickname="getSponsorGrantCalls")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns all the grant calls associated with a sponsor"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForGetSponsor
    @GetMapping("{id}/$GRANTCALL_PATH")
    fun getSponsorGrantCalls(@PathVariable id:Long,
                             @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                             @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                             @RequestParam(required=false) sorting:List<String>?): Page<GrantCallDTO>


    @ApiOperation(value = "Creates a new sponsor", nickname="createSponsor")
    @ApiResponses(value = [
        ApiResponse(code = 201, message = "Created successfully a new sponsor"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 409, message = "A sponsor already exists with the same id"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.CREATED)
    @AllowedForCreationSponsor
    @PostMapping("", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun createSponsor(@RequestPart sponsor:SponsorDTO, @RequestPart password:String): SponsorDTO?

    @ApiOperation(value = "Updates the sponsor with specified id", nickname="updateSponsor")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "The sponsor was updated successful"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "Sponsor with id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForUpdateSponsor
    @PutMapping("{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun updateSponsor(@PathVariable id:Long, @RequestBody sponsor:SponsorDTO)

    @ApiOperation(value = "Deletes a sponsor with specified id", nickname="deleteSponsor")
    @ApiResponses(value = [
        ApiResponse(code = 204, message = "Returns no content if sponsor deletion was successful"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404,message = "Sponsor with id doesnt exist"),
        ApiResponse(code = 500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowedForDeletionSponsor
    @DeleteMapping("{id}")
    fun deleteSponsor(@PathVariable id:Long)


    @ApiOperation(value = "Creates a new grant call associated to sponsor id")
    @ApiResponses(value = [
        ApiResponse(code = 201, message = "Successfully create a grant call by id given"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.CREATED)
    @AllowedForCreationGrantCall
    @PostMapping("{id}/$GRANTCALL_PATH", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun createGrantCall(@PathVariable id: Long,
                        @RequestPart("grantCall") grantCall:GrantCallDTO,
                        @RequestPart("evalPanel") evalPanel: EvaluationPanelDTO,
                        @RequestParam("reviewers") reviewers: List<Long>): GrantCallDTO?
}