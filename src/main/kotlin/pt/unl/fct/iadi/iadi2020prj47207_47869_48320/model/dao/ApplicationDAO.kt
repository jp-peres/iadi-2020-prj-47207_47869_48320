package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao

import java.time.LocalDate
import java.util.*
import javax.persistence.*

@Entity
@Table(name="application")
data class ApplicationDAO (
                           val submissionDate: Date,
                           var status:Status,
                           @ManyToOne @JoinColumn(name = "fk_stud",nullable = false)
                           val student:StudentDAO,
                           @ManyToOne @JoinColumn(name="fk_call", nullable=false)
                           val grant_call: GrantCallDAO,
                           @OneToMany(mappedBy = "app", cascade = [CascadeType.ALL], orphanRemoval = true)
                           val submissionItems:Set<SubmissionItemDAO>){
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val app_id:Long = 0
    enum class Status{
        ON_HOLD,
        ACCEPTED,
        DENIED
    }
    @OneToMany(mappedBy="idGA", cascade = [CascadeType.ALL], orphanRemoval = true)
    val evaluations:Set<EvaluationDAO> = emptySet()
}
