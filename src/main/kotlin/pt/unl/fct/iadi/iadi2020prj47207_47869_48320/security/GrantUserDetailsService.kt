package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.AuthUserDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.*

@Service
class GrantUserDetailsService(val authUserRepo:AuthUserRepository):UserDetailsService {
    override fun loadUserByUsername(username: String): UserDetails{
        return authUserRepo.findByUsername(username).map { GrantUserDetails(it) }.orElse(
                GrantUserDetails(AuthUserDAO("test","test",true,"ADMIN"))
        )
        /*.orElseThrow {
            UsernameNotFoundException("User was not found.")
        }*/
    }
}
