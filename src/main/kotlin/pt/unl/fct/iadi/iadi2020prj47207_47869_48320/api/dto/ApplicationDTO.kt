package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ApplicationDAO
import java.time.LocalDate
import java.util.*

data class ApplicationDTO(val id_app:Long, val submissionDate:Date, val status:String, val stud_id:Long,
                          val grant_id:Long, val submissionItems: List<SubmissionItemDTO>){
    constructor(it: ApplicationDAO):this(it.app_id,it.submissionDate,it.status.toString(),
            it.student.id,it.grant_call.id,it.submissionItems.map { it2 ->
        SubmissionItemDTO(it2.id,it2.submissionItemValue,it2.itemDataType,it2.app!!.app_id)
    })
}