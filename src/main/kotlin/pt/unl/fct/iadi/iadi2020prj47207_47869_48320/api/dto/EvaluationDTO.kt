package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.EvaluationDAO
import java.util.*

data class EvaluationDTO(val id:Long, val idReviewer:Long, val idGA:Long,
                         val points:Int, val notes:String, val date:String) {
    constructor(it:EvaluationDAO):this(it.id,it.reviewer.id,it.idGA.app_id,it.points,it.notes,it.date.toString())
}