package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ReviewerDAO

data class ReviewerDTO(override val id: Long, override val name: String, override val email: String,
                       override val idInst: Long, override val address: String,
                       override val phoneNumber: String): UserDTO {
    override val type: String = REVIEWER_TYPE
    companion object{
        const val REVIEWER_TYPE:String = "REVIEWER"
    }
    constructor(it:ReviewerDAO):this(it.id,it.name,it.email, it.inst.idInst,
            it.address,it.phoneNumber)
}