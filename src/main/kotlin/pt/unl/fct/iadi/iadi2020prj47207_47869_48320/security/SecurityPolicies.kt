package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security

import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited

/**
 * Role wide Conditions
 */
const val admin_Cond = "hasRole('ADMIN')"
const val stud_Cond = "hasRole('STUDENT')"
const val spo_Cond = "hasRole('SPONSOR')"
const val rev_Cond =  "hasRole('REVIEWER')"
const val inst_Cond1 = "(@secureServ.relationshipIsSame(#id,#stud.idInst) and $admin_Cond)"
const val inst_Cond2 = "(@secureServ.relationshipIsSame(#id,#rev.idInst) and $admin_Cond)"
const val app_Cond1 = "@secureServ.relationshipIsSame(#id,#app.stud_id)"

/**
 * Checks if current principal has specified role and if its the same user with specified id
 */
const val stud_Cond1 = "(@secureServ.principalIsStudent(authentication.name,#id) and $stud_Cond)"
const val rev_Cond1 = "(@secureServ.principalIsReviewer(authentication.name,#id) and $rev_Cond)"
const val spo_Cond1 = "(@secureServ.principalIsSponsor(authentication.name,#id) and $spo_Cond)"

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.TYPE, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
annotation class BaseAnnotation


/**
 * ADMIN role ONLY related annotations
 */

@BaseAnnotation
@PreAuthorize(admin_Cond)
annotation class AllowedForAdminOperations

@BaseAnnotation
@PreAuthorize(inst_Cond1)
annotation class AllowedForCreationStudent

@BaseAnnotation
@PreAuthorize(inst_Cond2)
annotation class AllowedForCreationReviewer

@BaseAnnotation
@PreAuthorize(admin_Cond)
annotation class AllowedForInstitutionOperations

@BaseAnnotation
@PreAuthorize(admin_Cond)
annotation class AllowedForApplicationOperations

/**
 * STUDENT role ONLY related annotations
 */
@BaseAnnotation
@PreAuthorize("$stud_Cond1 or $admin_Cond")
annotation class AllowedForGetStudent

@BaseAnnotation
@PreAuthorize(admin_Cond)
annotation class AllowedForGetAllStudents

@BaseAnnotation
@PreAuthorize("($stud_Cond1 and $app_Cond1) or $admin_Cond")
annotation class AllowedForCreationApplication

@BaseAnnotation
@PreAuthorize("$stud_Cond1 or $admin_Cond")
annotation class AllowedForGetOpenGrantCalls

@BaseAnnotation
@PreAuthorize("$stud_Cond1 or $admin_Cond")
annotation class AllowedForGetStudentApplications

@BaseAnnotation
@PreAuthorize("$stud_Cond1 or $admin_Cond")
annotation class AllowedForGetStudentApplication

@BaseAnnotation
@PreAuthorize("$stud_Cond1 or $admin_Cond")
annotation class AllowedForGetStudentApplicationReviews

@BaseAnnotation
@PreAuthorize("$stud_Cond1 or $admin_Cond")
annotation class AllowedForGetStudentApplicationStatus

@BaseAnnotation
@PreAuthorize(admin_Cond)
annotation class AllowedForDeletionStudent

/**
 * REVIEWER role ONLY related annotations
 */

@BaseAnnotation
@PreAuthorize(admin_Cond)
annotation class AllowedForGetAllReviewers

@BaseAnnotation
@PreAuthorize("$rev_Cond1 or $admin_Cond")
annotation class AllowedForGetReviewer

@BaseAnnotation
@PreAuthorize(admin_Cond)
annotation class AllowedForDeletionReviewer

/**
 * SPONSOR role ONLY related annotations
 */
@BaseAnnotation
@PreAuthorize("$spo_Cond1 or $admin_Cond")
annotation class AllowedForUpdateSponsor

@BaseAnnotation
@PreAuthorize(admin_Cond)
annotation class AllowedForGetAllSponsors

@BaseAnnotation
@PreAuthorize("$spo_Cond1 or $admin_Cond")
annotation class AllowedForGetSponsor

@BaseAnnotation
@PreAuthorize("$spo_Cond1 or $admin_Cond")
annotation class AllowedForCreationGrantCall

@BaseAnnotation
@PreAuthorize(admin_Cond)
annotation class AllowedForDeletionSponsor

@BaseAnnotation
@PreAuthorize(admin_Cond)
annotation class AllowedForCreationSponsor