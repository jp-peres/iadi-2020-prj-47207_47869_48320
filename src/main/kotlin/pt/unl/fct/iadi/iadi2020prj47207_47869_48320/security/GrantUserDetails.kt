package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.AuthUserDAO

data class GrantUserDetails(val user: AuthUserDAO):UserDetails{

    val enconder:PasswordEncoder = BCryptPasswordEncoder()

    val userName:String
    val accNotLocked:Boolean
    val accNotExpired:Boolean
    val enabled:Boolean
    val credentialsNotExp:Boolean
    val roles:MutableSet<GrantedAuthority>
    private var password:String

    init {
        this.userName = user.username
        this.accNotLocked = true
        this.accNotExpired = true
        this.enabled = user.active
        this.credentialsNotExp = true
        this.password = enconder.encode(user.password)
        val roles = user.roles.split("|")
        this.roles = roles.map{SimpleGrantedAuthority("ROLE_$it")}.toMutableSet()
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return this.roles
    }

    override fun getPassword(): String = this.password

    override fun getUsername(): String = this.userName

    override fun isAccountNonExpired(): Boolean = this.accNotExpired

    override fun isAccountNonLocked(): Boolean = this.accNotLocked

    override fun isCredentialsNonExpired(): Boolean = this.credentialsNotExp

    override fun isEnabled(): Boolean = this.enabled

}