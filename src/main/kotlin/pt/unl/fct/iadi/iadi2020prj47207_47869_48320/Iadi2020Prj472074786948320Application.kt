package pt.unl.fct.iadi.iadi2020prj47207_47869_48320

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.transaction.annotation.EnableTransactionManagement

@SpringBootApplication
@EnableTransactionManagement
class Iadi2020Prj472074786948320Application{
    companion object{
        const val RESOURCE:String = "managementsys"
        const val RESPONSE_BAD_REQUEST = "Invalid Request"//400
        const val SERVER_ERROR = "Server Error"
        const val UNAUTHORIZED = "Not Authenticated"//401
        const val FORBIDDEN = "Unauthorized but authenticated"//403
        const val RESPONSE_NOT_FOUND = "The resource you were trying to reach was not found"//404
        const val RESPONSE_CONFLICT = "The request could not be completed due to a conflict with the current state of the target resource"//409
        const val RESPONSE_SERVER_ERROR = "Server error..."//500
    }
}

fun main(args: Array<String>) {
    runApplication<Iadi2020Prj472074786948320Application>(*args)
}
