package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.controller

import org.springframework.data.domain.Page
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Student
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.GrantCallDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.StudentDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.*

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.UserService
import java.security.Principal

@RestController
class StudentController(val users: UserService): Student {

    override fun getAllStudents(pageNumber: Int, pageSize: Int,
                                sorting: List<String>?): Page<StudentDTO> =
            users.getAllStudents(pageNumber,pageSize,sorting)

    override fun getStudent(id: Long): StudentDTO? = users.getStudent(id)

    override fun removeStudent(id: Long) = users.removeStudent(id)

    override fun createApplication(id: Long,app: ApplicationDTO): ApplicationDTO? =
            users.createApplication(app)

    override fun getOpenGrantCalls(pageNumber: Int, pageSize: Int,
                                   sorting: List<String>?): Page<GrantCallDTO> =
        users.getOpenGrantCalls(pageNumber,pageSize,sorting)

    override fun getStudentApplications(id: Long, pageNumber: Int, pageSize: Int,
                                        sorting: List<String>?): Page<ApplicationDTO> =
            users.getStudentApplications(id,pageNumber,pageSize,sorting)

    override fun getStudentApplication(id: Long, id2: Long): ApplicationDTO = users.getStudentApplication(id,id2)

    override fun getReviewsOfApplicationOfStudent(id: Long, id2: Long, pageNumber: Int,
                                                  pageSize: Int,
                                                  sorting: List<String>?): Page<EvaluationDTO> =
            users.getReviewsOfApplicationOfStudent(id,id2,pageNumber,pageSize,sorting)

    override fun getStudentApplicationStatus(id: Long, id2: Long): String = users.getStudentApplicationStatus(id,id2)
}