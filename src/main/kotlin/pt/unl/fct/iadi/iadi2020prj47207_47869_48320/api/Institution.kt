package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api

import io.swagger.annotations.*
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.SERVER_ERROR
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.FORBIDDEN
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.UNAUTHORIZED
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.InstitutionDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.StudentDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Institution.Companion.INSTITUTION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Student.Companion.STUDENT_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Reviewer.Companion.REVIEWER_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.AllowedForCreationReviewer
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.AllowedForCreationStudent
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.AllowedForInstitutionOperations

@Api(value= INSTITUTION_PATH, description = "institution API", tags=["Institution"])
@RequestMapping("$RESOURCE/$INSTITUTION_PATH")
interface Institution {

    companion object {
        const val INSTITUTION_PATH: String = "institutions"
    }

    @ApiOperation(value = "Returns all the institutions from the system.", nickname="getAllInstitutions")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns all the institutions"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForInstitutionOperations
    @GetMapping("")
    fun getAllInstitutions(@RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                           @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                           @RequestParam(required=false) sorting:List<String>?):Page<InstitutionDTO>


    @ApiOperation(value = "Returns the institution with given id.", nickname="getInstitution")
    @ApiResponses(value = [
        ApiResponse(code = 200,message = "Returns the institution with the specified id"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "No institution with specified id was found"),
        ApiResponse(code = 500, message = SERVER_ERROR)

    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForInstitutionOperations
    @GetMapping("{id}")
    fun getInstitution(@PathVariable id:Long): InstitutionDTO?


    @ApiOperation(value = "Returns all the students from an institution", nickname="getInstitutionStudents")
    @ApiResponses(value = [
        ApiResponse(code=200,message = "Returns all the students from the institution"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404,message = "No institution with specified id was found"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForInstitutionOperations
    @GetMapping("{id}/$STUDENT_PATH")
    fun getInstitutionStudents(@PathVariable id:Long,
                               @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                               @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                               @RequestParam(required=false) sorting:List<String>?):Page<StudentDTO>


    @ApiOperation(value = "Returns all the reviewers from an institution", nickname="getInstitutionReviewers")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Returns all the reviewers from the institution"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "No institution with specified id was found"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForInstitutionOperations
    @GetMapping("{id}/$REVIEWER_PATH")
    fun getInstitutionReviewers(@PathVariable id:Long,
                                @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                                @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                                @RequestParam(required=false) sorting:List<String>?):Page<ReviewerDTO>


    @ApiOperation(value = "Creates a new institution", nickname="createInstitution")
    @ApiResponses(value = [
        ApiResponse(code = 201, message = "Created a new institution successfully"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 409, message = "An institution with the same id already exists"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.CREATED)
    @AllowedForInstitutionOperations
    @PostMapping("",consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun createInstitution(@RequestBody inst:InstitutionDTO): InstitutionDTO?


    @ApiOperation(value = "Updates the info of the target institution", nickname = "updateInstitution")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Updated the institution successfully"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "No institution with specified id was found"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @AllowedForInstitutionOperations
    @PutMapping("{id}",consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun updateInstitution(@PathVariable id:Long, @RequestBody inst:InstitutionDTO)


    @ApiOperation(value = "Deletes an institution with specified id", nickname="deleteInstitution")
    @ApiResponses(value = [
        ApiResponse(code=204,message = "Returns no content if institution deletion was successful"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404,message = "Reviewer with id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowedForInstitutionOperations
    @DeleteMapping("{id}")
    fun deleteInstitution(@PathVariable id:Long)

    @ApiOperation(value = "Creates a new student", nickname="addStudent")
    @ApiResponses(value = [
        ApiResponse(code = 201, message = "Created a new student successfully"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 409, message = "A student with the same id already exists"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.CREATED)
    @AllowedForCreationStudent
    @PostMapping("{id}/$STUDENT_PATH",consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun addStudent(@PathVariable id:Long, @RequestPart stud:StudentDTO, @RequestPart password:String):StudentDTO?


    @ApiOperation(value = "Creates a new reviewer", nickname="addReviewer")
    @ApiResponses(value = [
        ApiResponse(code = 201, message = "Created a new reviewer successfully"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "No institution with specified id was found"),
        ApiResponse(code = 409, message = "A reviewer with the same id already exists"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.CREATED)
    @AllowedForCreationReviewer
    @PostMapping("{id}/$REVIEWER_PATH",consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun addReviewer(@PathVariable id:Long, @RequestPart rev:ReviewerDTO, @RequestPart password:String):ReviewerDTO?
}