package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Evaluation.Companion.EVALUATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.EvaluationPanel.Companion.EVALUATION_PANEL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Reviewer.Companion.REVIEWER_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.FORBIDDEN
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESPONSE_NOT_FOUND
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.UNAUTHORIZED
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.SERVER_ERROR

@Api(value=EVALUATION_PANEL_PATH, description = "EvaluationPanel API", tags=["EvaluationPanel"])
@RequestMapping("$RESOURCE/$EVALUATION_PANEL_PATH")
interface EvaluationPanel {

    companion object{
        const val EVALUATION_PANEL_PATH = "evaluation_panels"
    }

    @ApiOperation("Retrieves the list of reviewers from the evaluation panel")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Successfully return data of the requested reviewers"),
        ApiResponse(code=401,message = UNAUTHORIZED),
        ApiResponse(code=403,message = FORBIDDEN),
        ApiResponse(code=404,message = "The application with id doesnt exist"),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{idEvalPanel}/$REVIEWER_PATH")
    fun getAllReviewers(@PathVariable idEvalPanel:Long,
                        @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                        @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                        @RequestParam(required=false) sorting:List<String>?): Page<ReviewerDTO?>

    @ApiOperation("Retrieves the list of evaluation made by the reviewers from the evaluation panel")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Successfully return data of the requested evaluations"),
        ApiResponse(code=401, message = UNAUTHORIZED),
        ApiResponse(code=403, message = FORBIDDEN),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{idEvalPanel}/$EVALUATION_PATH")
    fun getAllEvaluations(@PathVariable idEvalPanel: Long,
                          @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                          @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                          @RequestParam(required=false) sorting:List<String>?): Page<EvaluationDTO>

    @ApiOperation("Retrieves info of the panel chair from the evaluation panel specified")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Successfully return data of the panel chair"),
        ApiResponse(code=401, message = UNAUTHORIZED),
        ApiResponse(code=403, message = FORBIDDEN),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{idEvalPanel}/chair")
    fun getPanelChair(@PathVariable idEvalPanel: Long): ReviewerDTO?

    @ApiOperation("Disassembles the evaluation panel with the assigned id")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Removed evaluation panel with success"),
        ApiResponse(code=401, message = UNAUTHORIZED),
        ApiResponse(code=403, message = FORBIDDEN),
        ApiResponse(code=404, message = RESPONSE_NOT_FOUND),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("{id}")
    fun dismantleEvaluationPanel(@PathVariable id: Long)

    @ApiOperation("The respective panel makes a decision on the application")
    @ApiResponses(value=[
        ApiResponse(code=200, message = "Decision made with success"),
        ApiResponse(code=401, message = UNAUTHORIZED),
        ApiResponse(code=403, message = FORBIDDEN),
        ApiResponse(code=404, message = RESPONSE_NOT_FOUND),
        ApiResponse(code=500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @PostMapping("{id}/application/{idGA}")
    fun decideApplication(@PathVariable id:Long, @PathVariable idGA: Long, @RequestBody decision:Boolean)

}