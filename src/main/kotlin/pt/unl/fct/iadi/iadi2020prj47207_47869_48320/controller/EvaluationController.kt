package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.controller

import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Evaluation
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.EvaluationService

@RestController
class EvaluationController(val evaluations:EvaluationService):Evaluation {

    override fun getEvaluationsOfApplication(id: Long, pageNumber: Int,
                                             pageSize: Int, sorting: List<String>?): Page<EvaluationDTO> =
            evaluations.getEvaluationsOfApplication(id,pageNumber,pageSize,sorting)

    override fun getEvaluationOfAppFromReviewer(idReviewer: Long, idGA: Long): EvaluationDTO? = evaluations.getEvaluationOfAppFromReviewer(idReviewer, idGA)
    /***/
    override fun getEvaluationFromEvalPanel(idPanel: Long): Collection<EvaluationDTO> = emptyList()

    override fun getEvaluationsFromCall(idGC: Long): Collection<EvaluationDTO> = evaluations.getEvaluationsFromCall(idGC)

    override fun deleteEvaluation(id: Long){
        evaluations.deleteEvaluation(id)
    }

}