package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.GrantCallDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESPONSE_NOT_FOUND
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.FORBIDDEN
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.UNAUTHORIZED
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.SERVER_ERROR
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Application.Companion.APPLICATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.GrantCall.Companion.GRANTCALL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Student.Companion.STUDENT_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.RequestItemDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.EvaluationPanel.Companion.EVALUATION_PANEL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationPanelDTO

@Api(value="GrantCall", description = "GrantCall API", tags=["GrantCall"])
@RequestMapping("$RESOURCE/$GRANTCALL_PATH")
interface GrantCall {

    companion object {
        const val GRANTCALL_PATH: String = "grant_calls"
    }

    @ApiOperation(value = "Retrieves list of all grant calls.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved list of grant calls"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("")
    fun getAllGrantCalls(@RequestParam(required = false) status:String?,
                         @RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                         @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                         @RequestParam(required=false) sorting:List<String>?): Page<GrantCallDTO>

    @ApiOperation(value = "Retrieves list of all open grant calls")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved list of opened grant calls"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(STUDENT_PATH)
    fun getOpenGrantCalls(@RequestParam(required=false, defaultValue = "0") pageNumber:Int,
                          @RequestParam(required=false, defaultValue = "10") pageSize:Int,
                          @RequestParam(required=false) sorting:List<String>?):Page<GrantCallDTO>

    @ApiOperation(value = "Get grant call of evaluation panel")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved grant call of evaluation panel given"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{id}/$EVALUATION_PANEL_PATH")
    fun getEvalPanelGrantCall(@PathVariable id:Long): EvaluationPanelDTO?

    @ApiOperation(value = "Retrieves a grant call from specified id")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved the grant call by id given"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{id}")
    fun getGrantCall(@PathVariable id:Long): GrantCallDTO?

    @ApiOperation(value = "Updates info about a grant call")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Updated grant call with specified id successfully."),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun updateGrantCall(@PathVariable("id") id: Long, @RequestBody grantCall:GrantCallDTO)

    @ApiOperation(value = "Deletes a grant call by specified id")
    @ApiResponses(value = [
        ApiResponse(code = 204, message = "Successfully delete a grant call by id given"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "GrantCall with specified id was not found"),
        ApiResponse(code = 500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{id}")
    fun deleteGrantCall(@PathVariable id:Long)

    @ApiOperation(value = "Delete request item")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully delete request item"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 500,message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{id}/items/{item_id}")
    fun deleteRequestItem(@PathVariable id:Long, @PathVariable item_id:Long)

    @ApiOperation(value = "Add a new item to the grant list given by id as parameter")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully add an item"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = "GrantCall with specified id doesnt exist"),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @PostMapping("{id}/items",consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun addRequestItem(@PathVariable id:Long,@RequestBody reqItem:RequestItemDTO): RequestItemDTO?

    @ApiOperation(value = "Updates an existing requestItem with id {id2} from GrantCall with id {id}")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully updated the item"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = RESPONSE_NOT_FOUND),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("{id}/items/{id2}",consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun updateRequestItem(@PathVariable id:Long,@PathVariable id2:Long, @RequestBody reqItem:RequestItemDTO)

    @ApiOperation(value = "Get request items of grant call given by id as parameter")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved a list of items of grant call"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = RESPONSE_NOT_FOUND),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{id}/items")
    fun getRequestItems(@PathVariable id:Long):List<RequestItemDTO>?


    @ApiOperation(value = "Retrieves all existing request items")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved list of all request items"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = RESPONSE_NOT_FOUND),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("items")
    fun getAllItems():List<RequestItemDTO>

    @ApiOperation(value = "Retrieves all applications for a given grantCall")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved list of all applications for grantCall id"),
        ApiResponse(code = 401, message = UNAUTHORIZED),
        ApiResponse(code = 403, message = FORBIDDEN),
        ApiResponse(code = 404, message = RESPONSE_NOT_FOUND),
        ApiResponse(code = 500, message = SERVER_ERROR)
    ])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{id}/$APPLICATION_PATH")
    fun getGrantCallApplications(@PathVariable id:Long):List<ApplicationDTO>

}