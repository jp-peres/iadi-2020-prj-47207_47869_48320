package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao

import javax.persistence.*


// TODO: make usernames not have collisions
@Entity
@Table(name="auth_user")
data class AuthUserDAO(val username:String,
                       val password:String,
                       val active:Boolean,
                       val roles:String){
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id:Long=0
}