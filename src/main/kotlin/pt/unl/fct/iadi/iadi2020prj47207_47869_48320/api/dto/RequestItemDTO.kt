package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto

import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.RequestItemDAO

data class RequestItemDTO(val id:Long, val item_name:String, val required: Boolean, val type:String){
    constructor(it:RequestItemDAO):this(it.id,it.item_name,it.required,it.dataType)
}