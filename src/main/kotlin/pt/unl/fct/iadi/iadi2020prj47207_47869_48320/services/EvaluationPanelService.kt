package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services

import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ApplicationDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.ApplicationRepository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.EvaluationPanelRepository

@Service
class EvaluationPanelService(val evalPalRepo: EvaluationPanelRepository,
                             val appRepo: ApplicationRepository) {

    fun getAllReviewers(idEvalPanel:Long, pageNumber: Int,
                        pageSize: Int, sorting: List<String>?): Page<ReviewerDTO?> {
        val pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return evalPalRepo.getReviewersFromPanel(idEvalPanel,pageable).map { ReviewerDTO(it) }
    }

    fun checkIfReviewerBelongsToPanel(idRev:Long,idEvalPanel: Long):Boolean{
        return evalPalRepo.reviewerBelongsToPanel(idRev,idEvalPanel)
    }

    fun getAllEvaluations(idEvalPanel: Long, pageNumber: Int,
                          pageSize: Int, sorting: List<String>?): Page<EvaluationDTO> {
        val pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return evalPalRepo.getEvaluationsFromPanel(idEvalPanel,pageable).map { EvaluationDTO(it) }
    }

    fun getAllApplicationsFromPanel(idPanel:Long, pageNumber: Int, pageSize: Int,
                                    sorting: List<String>?):Page<ApplicationDTO> {
        if (evalPalRepo.existsById(idPanel)) {
            val pageable =  PageableBuilder.build(pageNumber,pageSize,sorting)
            return evalPalRepo.getApplicationsFromPanel(idPanel,pageable).map { ApplicationDTO(it) }
        }
        else
            throw NotFoundException("Evaluation panel not found","Evaluation panel with id $idPanel was not found")

    }

    fun getApplicationFromPanel(idPanel:Long, idGA: Long):ApplicationDTO{
        val dao = evalPalRepo.getApplicationFromPanel(idPanel,idGA).orElseThrow {
            NotFoundException("GrantApplication not found","GrantApplication with $idGA was not found")
        }
        return ApplicationDTO(dao)
    }

    fun getPanelChair(idEvalPanel: Long): ReviewerDTO? {
        val chair = evalPalRepo.getChairmanOfPanel(idEvalPanel)
        if (chair == null)
            throw NotFoundException("Evaluation Panel not Found","No Evaluation Panel with specified id $idEvalPanel was found.")
        else
            return ReviewerDTO(chair)
    }

    //fun getSponsor() {}
    @Transactional
    fun decideApplication(id:Long, idGA:Long, approve: Boolean){
        val app = evalPalRepo.getApplicationFromPanel(id,idGA).orElseThrow {
            throw NotFoundException("EvaluationPanel/GrantApplication not found", "EvaluationPanel/GrantApplication with id's ($id,$idGA) was not found")
        }
        app.status = if(approve) ApplicationDAO.Status.ACCEPTED else ApplicationDAO.Status.DENIED
        appRepo.save(app)
    }

    @Transactional
    fun dismantleEvaluationPanel(id: Long) {
        if(evalPalRepo.existsById(id))
            evalPalRepo.deleteById(id)
        else
            throw NotFoundException("No EvaluationPanel Found","No EvaluationPanel with specified id '$id' was found.")
    }
}