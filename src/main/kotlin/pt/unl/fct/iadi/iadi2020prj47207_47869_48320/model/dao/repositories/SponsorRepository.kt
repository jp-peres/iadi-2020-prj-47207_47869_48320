package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.GrantCallDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.SponsorDAO

@Repository
interface SponsorRepository: JpaRepository<SponsorDAO, Long> {
    @Query(value="select s.grantCalls from SponsorDAO s where s.id =: id")
    fun getGrantCallsBySponsorId(@Param(value = "id") id:Long, pageable: Pageable): Page<GrantCallDAO>
}