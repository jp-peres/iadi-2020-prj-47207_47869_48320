package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.controller
import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.RestController
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Institution
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.InstitutionDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.StudentDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.InstitutionService
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.UserService

@RestController
class InstitutionController(val institutionService:InstitutionService,
                            val usersService:UserService) : Institution {

    override fun addStudent(id: Long, stud: StudentDTO, password: String): StudentDTO? =
            usersService.addStudent(stud,password)

    override fun addReviewer(id: Long, rev: ReviewerDTO, password: String): ReviewerDTO? =
            usersService.addReviewer(rev,password)

    override fun getAllInstitutions(pageNumber:Int, pageSize:Int, sorting:List<String>?): Page<InstitutionDTO> =
            institutionService.getAllInstitutions(pageNumber,pageSize,sorting)

    override fun getInstitution(id: Long): InstitutionDTO? =
        institutionService.getInstitution(id)

    override fun getInstitutionStudents(id: Long, pageNumber:Int, pageSize:Int,
                                        sorting:List<String>?): Page<StudentDTO> =
            institutionService.getInstitutionStudents(id,pageNumber,pageSize,sorting)

    override fun getInstitutionReviewers(id: Long, pageNumber:Int, pageSize:Int,
                                         sorting:List<String>?): Page<ReviewerDTO> =
            institutionService.getInstitutionReviewers(id,pageNumber,pageSize,sorting)

    override fun createInstitution(inst:InstitutionDTO): InstitutionDTO? =
            institutionService.createInstitution(inst)

    override fun updateInstitution(id: Long, inst: InstitutionDTO) =
            institutionService.updateInstitution(id,inst)

    override fun deleteInstitution(id: Long) =
            institutionService.deleteInstitution(id)
}