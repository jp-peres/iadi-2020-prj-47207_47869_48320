package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao

import javax.persistence.*

@Entity
@Table(name="sponsor")
data class SponsorDAO(var name:String, var email:String, var contact:String){
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id:Long = 0
    @OneToMany(mappedBy = "sponsor",cascade = [CascadeType.ALL], orphanRemoval = true)
    val grantCalls:MutableSet<GrantCallDAO> = mutableSetOf()
}