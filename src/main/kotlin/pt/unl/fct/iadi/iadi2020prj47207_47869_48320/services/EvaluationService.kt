package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Evaluation
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.EvaluationDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.ApplicationRepository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.EvaluationRepository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.ReviewerRepository
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

@Service
class EvaluationService(val evaluations: EvaluationRepository,
                        val reviewers: ReviewerRepository,
                        val applications: ApplicationRepository) {

    @Transactional
    fun submitEvaluation(evaluation: EvaluationDTO): EvaluationDTO? {
        val daoRev = reviewers.findById(evaluation.idReviewer).orElseThrow { throw NotFoundException("HI","BYE") }
        val daoApp = applications.findById(evaluation.idGA).orElseThrow { throw NotFoundException("HI","BYE") }
        val dao = evaluations.save(EvaluationDAO(evaluation.idReviewer, daoRev, daoApp, evaluation.points, evaluation.notes,
                Date.from(LocalDate.parse(evaluation.date, DateTimeFormatter.ISO_DATE).atStartOfDay(ZoneId.systemDefault()).toInstant()))
        )
        return EvaluationDTO(dao)
    }

    fun getEvaluationsOfApplication(idGA: Long, pageNumber: Int, pageSize: Int,
                                    sorting: List<String>?): Page<EvaluationDTO> {
        val pageable: Pageable = PageableBuilder.build(pageNumber,pageSize,sorting)
        return evaluations.findByGrantApplication(idGA,pageable).map { EvaluationDTO(it) }
    }

    fun getEvaluationOfAppFromReviewer(idReviewer: Long, idGA: Long): EvaluationDTO? {
        //evaluations.filterValues { it.idReviewer == idReviewer && it.idGA == idGA }.values.firstOrNull()
        return EvaluationDTO(evaluations.findByGrantApplicationAndReviewer(idReviewer, idGA).orElseThrow { throw NotFoundException("HI","BYE") })
    }

    /**
     * @deprecated ignore functionality
    fun getEvaluationFromEvalPanel(idPanel: Number): Collection<EvaluationDTO> =
    evaluations.filterValues { false }.values
     */

    fun getEvaluationsFromCall(idGC: Long): Collection<EvaluationDTO> {
        //return evaluations.filterValues { outerIt -> appServ.getAllApplications().any { it.grant_id == idGC && it.id_app == outerIt.idGA } }.values
        return evaluations.findByGrantCall(idGC).map { EvaluationDTO(it) }
    }

    @Transactional
    fun deleteEvaluation(id: Long) {
        if (evaluations.existsById(id))
            evaluations.deleteById(id)
        else
            throw NotFoundException("No Evaluation Found","No Evaluation with specified id '$id' was found.")
    }
}