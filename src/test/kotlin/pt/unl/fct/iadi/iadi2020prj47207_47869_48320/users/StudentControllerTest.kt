package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.users

import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Application.Companion.APPLICATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Student.Companion.STUDENT_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ApplicationDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.StudentDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ApplicationDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.GrantCallDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.InstitutionDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.StudentDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.JwtUtils
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.SecurityService
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.UserService
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.sponsor.SponsorControllerTest

@SpringBootTest
@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
class StudentControllerTest{

    @Autowired
    lateinit var mvc: MockMvc
    @MockBean
    lateinit var userServ: UserService

    @MockBean
    lateinit var secureServ: SecurityService

    companion object{
        private val inst1 = InstitutionDAO("FCT",420, "Cap","N/A")
        private val inst2 = InstitutionDAO("UNL",1337, "Lis","N/H")
        val student1 = StudentDAO("bica", "bica@bica.bica", inst1, "Entrocar", "9177")
        private val student2 = StudentDAO("tiago", "tiago@tiago.tiago", inst2, "Enbarqar", "9669")

        val studentPool = setOf(student1, student2)
        val objmap = ObjectMapper()
        val pageable = PageRequest.of(0,15)
        val studPage = PageImpl(studentPool.toList(), pageable,2)
        val token1 = JwtUtils.mockJwt("test","ROLE_ADMIN")
        val token2 = JwtUtils.mockJwt("bica","ROLE_STUDENT")
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun getAllStudents(){
        Mockito.`when`(userServ.getAllStudents(0,15,null)).thenReturn(studPage.map { StudentDTO(it) })
        mvc.perform(get("/$RESOURCE/$STUDENT_PATH")
                .header("Authorization", token1))
                .andExpect(status().isOk)
                //.andExpect(jsonPath("$", Matchers.hasSize<Any>(studentPool.size)))
    }

    @WithMockUser(username = "bica", password = "test", roles = ["STUDENT"])
    @Test
    fun getStudent(){
            Mockito.`when`(secureServ.principalIsStudent("bica",0L)).thenReturn(true)
            Mockito.`when`(userServ.getStudent(student1.id)).thenReturn(StudentDTO(student1))
            mvc.perform(get("/$RESOURCE/$STUDENT_PATH/{id}", student1.id)
                    .header("Authorization", token2)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .content(objmap.writeValueAsString(StudentDTO(student1))))
                    .andExpect(status().isOk)
                    .andExpect(content().json(objmap.writeValueAsString(StudentDTO(student1))))
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun removeStudent() {
        studentPool.forEach {
            mvc.perform(delete("/$RESOURCE/$STUDENT_PATH/{id}",it.id)
                    .header("Authorization", token1))
                    .andExpect(status().isNoContent)
        }
    }

    //TODO: do applications tests (requires grantcalls etc.. will do next week)
    /*
    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun getStudentApplications(){
        studentPool.forEach {
            Mockito.`when`(userServ.getStudentApplications(it.id,0,15,null))
                    .thenReturn()
            mvc.perform(get("/$RESOURCE/$STUDENT_PATH/${it.id}/$APPLICATION_PATH"))
                    .andExpect(status().isOk)
                    .andExpect(jsonPath("$", Matchers.hasSize<Any>(it.applications.size)))
        }
    }*/

}