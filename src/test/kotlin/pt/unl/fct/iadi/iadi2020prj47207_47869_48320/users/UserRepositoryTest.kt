package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.users

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.InstitutionDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ReviewerDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.StudentDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.InstitutionRepository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.ReviewerRepository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.StudentRepository

@SpringBootTest
@ExtendWith(SpringExtension::class)
class
UserRepositoryTest {

    @Autowired
    lateinit var instRepo: InstitutionRepository

    @Autowired
    lateinit var studRepo: StudentRepository

    @Autowired
    lateinit var revRepo: ReviewerRepository

    companion object{
        var inst1 = InstitutionDAO("FCT",420, "Cap","N/A")
        var inst2 = InstitutionDAO("UNL",1337, "Lis","N/H")
        var student1 = StudentDAO("bica", "bica@bica.bica", inst1, "Entrocar", "9177")
        var student2 = StudentDAO("tiago", "tiago@tiago.tiago", inst2, "Enbarqar", "9669")
        var reviewer1 = ReviewerDAO("draco", "draco@draco.draco", inst1, "Entrocar", "9177")
        var reviewer2 = ReviewerDAO("pere", "pere@pere.pere", inst2, "Enbarqar", "9669")
    }

    @BeforeEach
    @Transactional
    fun `prepare for test`(){
        inst1 = instRepo.save(inst1)
        inst2 = instRepo.save(inst2)
        student1 = StudentDAO("bica", "bica@bica.bica", inst1, "Entrocar", "9177")
        student2 = StudentDAO("tiago", "tiago@tiago.tiago", inst2, "Enbarqar", "9669")
        reviewer1 = ReviewerDAO("draco", "draco@draco.draco", inst1, "Entrocar", "9177")
        reviewer2 = ReviewerDAO("pere", "pere@pere.pere", inst2, "Enbarqar", "9669")
    }

    @AfterEach
    @Transactional
    fun `clean up test`(){
        studRepo.deleteAll()
        revRepo.deleteAll()
        instRepo.deleteAll()
    }


    @Test
    @Transactional
    fun `insert & get students`(){
        assertEquals(0, studRepo.count())
        val id1 = studRepo.save(student1).id
        assertEquals(1, studRepo.count())
        val id2 = studRepo.save(student2).id
        assertEquals(2, studRepo.count())
        assertEquals(true, studRepo.existsById(id1))
        assertEquals(true, studRepo.existsById(id2))
    }

    @Test
    @Transactional
    fun `insert & get reviewers`(){
        assertEquals(0, revRepo.count())
        val id1 = revRepo.save(reviewer1).id
        assertEquals(1, revRepo.count())
        val id2 = revRepo.save(reviewer2).id
        assertEquals(2, revRepo.count())
        assertEquals(true, revRepo.existsById(id1))
        assertEquals(true, revRepo.existsById(id2))
    }

    @Test
    @Transactional
    fun `deleting students`(){
        assertEquals(0, studRepo.count())
        assertEquals(2, instRepo.count())
        student1 = studRepo.save(student1)
        assertEquals(student1, studRepo.findById(student1.id).get())
        studRepo.deleteById(student1.id)
        assertEquals(false, studRepo.findById(student1.id).isPresent)

        student2 = studRepo.save(student2)
        assertEquals(true, instRepo.existsById(student2.inst.idInst))

        /**
         * To establish relationship between both student and institution
         */
        inst2.users.add(student2)

        instRepo.deleteById(student2.inst.idInst)
        assertEquals(1, instRepo.count())
        assertEquals(false, studRepo.existsById(student2.id))
    }

    @Test
    @Transactional
    fun `deleting reviewers`(){
        assertEquals(0, revRepo.count())
        assertEquals(2, instRepo.count())
        reviewer1 = revRepo.save(reviewer1)
        assertEquals(reviewer1, revRepo.findById(reviewer1.id).get())
        revRepo.deleteById(reviewer1.id)
        assertEquals(false, studRepo.findById(reviewer1.id).isPresent)

        reviewer2 = revRepo.save(reviewer2)
        assertEquals(true, instRepo.existsById(reviewer2.inst.idInst))

        /**
         * To establish relationship between both reviewer and institution
         */
        inst2.users.add(reviewer2)

        instRepo.deleteById(reviewer2.inst.idInst)
        assertEquals(1, instRepo.count())
        assertEquals(false, revRepo.existsById(reviewer2.id))
    }

}