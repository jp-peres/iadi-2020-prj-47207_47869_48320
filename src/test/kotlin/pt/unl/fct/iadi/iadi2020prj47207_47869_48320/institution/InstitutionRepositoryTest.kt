package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.institution

import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.junit.FixMethodOrder
import org.junit.jupiter.api.AfterEach
import org.junit.runners.MethodSorters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.PageRequest
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.InstitutionDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ReviewerDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.StudentDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.*
import javax.transaction.Transactional


@SpringBootTest
@ExtendWith(SpringExtension::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class InstitutionRepositoryTest{

    @Autowired
    lateinit var instRepo: InstitutionRepository

    @Autowired
    lateinit var studRepo: StudentRepository

    @Autowired
    lateinit var revRepo: ReviewerRepository

    companion object {
        val inst1 = InstitutionDAO("FCT", 1234567, "Monte da Caparica", "919243123")
        val inst2 = InstitutionDAO("IPS", 3454444, "Setúbal", "23123124")
        val inst3 = InstitutionDAO("IST", 4949234, "Lisboa", "911")
        val inst4 = InstitutionDAO("ISEL", 4949234, "Lisboa", "911")


        val listInst = listOf(inst1, inst2, inst3)
    }

    @AfterEach
    fun `clean up tests`(){
        instRepo.deleteAll()
    }

    @Test
    @Transactional
    fun test1getAllInstitutions() {
        instRepo.save(inst1)
        instRepo.save(inst2)
        instRepo.save(inst3)

        var insts = instRepo.findAll().toList()
        //assertEquals(insts, listInst)
        assertThat(insts).containsExactlyInAnyOrderElementsOf(listInst)
        instRepo.deleteAll()

        insts = instRepo.findAll().toList()
        assertEquals(insts, emptyList<InstitutionDAO>())
    }

    @Test
    @Transactional
    fun test2getInstitutionTest(){
        /**to keep the spirit of original*/
        instRepo.save(inst1)
        instRepo.save(inst2)
        instRepo.save(inst3)

        val inst3ID = instRepo.save(inst3).idInst
        //4->inst3ID
        val instFound = instRepo.findById(inst3ID).get()
        assertEquals(instFound, inst3)
        //2??->0 cz idk what was 2 doing
        val instNotFound = instRepo.findById(0).orElse(null)
        assertEquals(instNotFound,null)
    }

    @Test
    @Transactional
    fun test3createInstitutionTest(){
        val inst4ID = instRepo.save(inst4).idInst
        //5->inst4ID
        val instFound = instRepo.findById(inst4ID).get()
        assertEquals(instFound, inst4)
    }


    // Using transactional due to lazy loading on applications otherwise we get exception
    @Test
    @Transactional
    fun test4getInstitutionStudents(){
        //was empty before
        val inst4 = instRepo.save(inst4)
        val stud1 = StudentDAO("joao peres","jp@fct.unl.pt",inst4,"stuff","123124")
        val stud2 = StudentDAO("joao oliveira","jgcd@fct.unl.pt",inst4,"stuff","123124")
        val studList = setOf(stud1,stud2)

        val non_studs = instRepo.getInstitutionStudentsFromId(inst4.idInst,PageRequest.of(0,15))
        assertEquals(emptyList<StudentDAO>(),non_studs.content)

        studRepo.save(stud1)
        studRepo.save(stud2)

        val studs =  instRepo.getInstitutionStudentsFromId(inst4.idInst,PageRequest.of(0,15))
        //assertEquals(studs,studList)
        assertThat(studs).containsExactlyInAnyOrderElementsOf(studList)
    }

    @Test
    @Transactional
    fun test5getInstitutionReviewers() {
        //was empty before and 5->inst4.idInst
        val inst4 = instRepo.save(inst4)

        val rev1 = ReviewerDAO("joao peres", "jp@fct.unl.pt", inst4, "stuff", "123124")
        val rev2 = ReviewerDAO("joao oliveira", "jgcd@fct.unl.pt", inst4, "stuff", "123124")
        val revList = setOf(rev1, rev2)

        val non_revs = instRepo.getInstitutionReviewersById(inst4.idInst,PageRequest.of(0,15))
        assertEquals(emptyList<ReviewerDAO>(),non_revs.content)

        revRepo.save(rev1)
        revRepo.save(rev2)

        val revs = instRepo.getInstitutionReviewersById(inst4.idInst,PageRequest.of(0,15))
        //assertEquals(revList, revs)
        assertThat(revList).containsExactlyInAnyOrderElementsOf(revs)
    }

    @Test
    @Transactional
    fun test6deleteInstitution(){
        //was empty before and 5->inst4.idInst
        val inst4 = instRepo.save(inst4)

        val rev1 = ReviewerDAO( "joao peres", "jp@fct.unl.pt", inst4, "stuff", "123124")
        val rev2 = ReviewerDAO( "joao oliveira", "jgcd@fct.unl.pt", inst4, "stuff", "123124")
        val stud1 = StudentDAO("joao peres","jp@fct.unl.pt",inst4,"stuff","123124")
        val stud2 = StudentDAO("joao oliveira","jgcd@fct.unl.pt",inst4,"stuff","123124")
        val studList = setOf(stud1,stud2)
        val revList = setOf(rev1, rev2)

        studRepo.save(stud1)
        studRepo.save(stud2)
        revRepo.save(rev1)
        revRepo.save(rev2)

        //represent relation
        inst4.users.addAll(listOf(stud1,stud2,rev1,rev2))

        var studs = instRepo.getInstitutionStudentsFromId(inst4.idInst,PageRequest.of(0,15))
        var revs = instRepo.getInstitutionReviewersById(inst4.idInst,PageRequest.of(0,15))

        assertEquals(studs.content.size, studList.size)
        assertEquals(revs.content.size, revList.size)

        instRepo.deleteById(inst4.idInst)


        val inst = instRepo.findById(inst4.idInst).orElse(null)

        assertEquals(null,inst)

        studs = instRepo.getInstitutionStudentsFromId(inst4.idInst,PageRequest.of(0,15))

        assertEquals(emptyList<StudentDAO>(),studs.content)

        revs = instRepo.getInstitutionReviewersById(inst4.idInst,PageRequest.of(0,15))

        assertEquals(emptyList<ReviewerDAO>(),revs.content)
    }
}