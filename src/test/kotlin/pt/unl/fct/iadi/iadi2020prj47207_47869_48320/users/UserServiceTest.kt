package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.users

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.InstitutionDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ReviewerDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.StudentDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.UserService

@SpringBootTest
@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
class UserServiceTest {

    @Test
    fun addStudentTest(){}
    @Test
    fun removeStudentTest() {}
    @Test
    fun getStudentApplicationsTest(){}
    @Test
    fun getAllStudentsTest(){}
    @Test
    fun getStudentTest(){

    }
    @Test
    fun addReviewerTest(){}
    @Test
    fun removeReviewerTest() {}
    @Test
    fun getAllReviewersTest(){}
    @Test
    fun getReviewerTest(){}
}