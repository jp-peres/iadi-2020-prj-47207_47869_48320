package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.users

import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Reviewer.Companion.REVIEWER_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.InstitutionDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ReviewerDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.JwtUtils
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.SecurityService
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.UserService
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.sponsor.SponsorControllerTest

@SpringBootTest
@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
class ReviewerControllerTest {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var userServ: UserService

    @MockBean
    lateinit var secureServ: SecurityService

    companion object {
        val inst1 = InstitutionDAO("FCT", 420, "Cap", "N/A")
        val inst2 = InstitutionDAO("UNL", 1337, "Lis", "N/H")
        val reviewer1 = ReviewerDAO("draco", "draco@draco.draco", inst1, "Entrocar", "9177")
        val reviewer2 = ReviewerDAO("pere", "pere@pere.pere", inst2, "Enbarqar", "9669")
        val reviewerPool = setOf(reviewer1, reviewer2)
        val pageable = PageRequest.of(0,15)
        val pageRev = PageImpl(reviewerPool.toList(), pageable, reviewerPool.size.toLong())
        val objmap = ObjectMapper()
        val token1 = JwtUtils.mockJwt("test","ROLE_ADMIN")
        val token2 = JwtUtils.mockJwt("draco","ROLE_REVIEWER")
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun getAllReviewersTest() {
        Mockito.`when`(userServ.getAllReviewers(0,15,null)).thenReturn(pageRev.map { ReviewerDTO(it) })
        mvc.perform(get("/$RESOURCE/$REVIEWER_PATH")
                .header("Authorization", token1))
                .andExpect(status().isOk)
                //.andExpect(jsonPath("$", Matchers.hasSize<Any>(reviewerPool.size)))
    }

    @WithMockUser(username = "draco", password = "test", roles = ["REVIEWER"])
    @Test
    fun getReviewerTest() {
            Mockito.`when`(secureServ.principalIsReviewer("draco",0L)).thenReturn(true)
            Mockito.`when`(userServ.getReviewer(reviewer1.id)).thenReturn(ReviewerDTO(reviewer1))
            mvc.perform(get("/$RESOURCE/$REVIEWER_PATH/{id}", reviewer1.id)
                    .header("Authorization", token2))
                    .andExpect(status().isOk)
                    .andExpect(content().json(objmap.writeValueAsString(ReviewerDTO(reviewer1))))
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun removeReviewerTest() {
        reviewerPool.forEach {
            mvc.perform(delete("/$RESOURCE/$REVIEWER_PATH/{id}", it.id)
                    .header("Authorization", token1))
                    .andExpect(status().isNoContent)
        }
    }
}