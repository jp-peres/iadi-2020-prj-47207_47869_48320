package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.application

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Application.Companion.APPLICATION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.ApplicationDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.JwtUtils
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.ApplicationService
import java.util.*

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
class ApplicationControllerTest {

    @MockBean
    lateinit var appService:ApplicationService

    @Autowired
    lateinit var mvc:MockMvc

    companion object{
        var object_mapper = ObjectMapper()

        const val URL_PATH = "/$RESOURCE/$APPLICATION_PATH"

        val request_item_1 = RequestItemDTO(1L, "CV", true, "Object-1")
        val request_item_2 = RequestItemDTO(11L, "Certificate of something", false, "Object-1")
        val reqi_call = listOf(request_item_1, request_item_2)
        val call = GrantCallDTO(1L, 1L, 1L, "Grant Call A-class", "Join us ;^)",
                "CV and Certificate of something needed.", 2020F, "2020-10-28", "2020-11-28", reqi_call)

        val student = StudentDTO(1L,"Joao","joka@fct.unl.pt",1,"Lisboa","912345678")
        val student_2 = StudentDTO(2L,"Goncalo","goka@fct.unl.pt",1,"Lisboa","923456789")

        val subi = SubmissionItemDTO(1L, "CV", "Object-1", 1L)
        val subi_2 = SubmissionItemDTO(2L, "CV", "Object-1", 2L)
        val subi_all_1 = mutableListOf<SubmissionItemDTO>(subi)
        val subi_all_2 = mutableListOf<SubmissionItemDTO>(subi_2)

        var stat = ApplicationDAO.Status.ON_HOLD.toString()

        val app_1 = ApplicationDTO(1L, Date(), stat, student.id, call.id, subi_all_1)

        val app_2 = ApplicationDTO(2L, Date(), stat, student_2.id, call.id, subi_all_2)
        val token = JwtUtils.mockJwt("test","ROLE_ADMIN")
    }

    //TODO: date messed up
    @Test
    @Disabled
    fun `test on getApplication`(){
        Mockito.`when`(appService.getApplication(app_1.id_app))
                .thenReturn(app_1)
        mvc.perform(get("$URL_PATH/${app_1.id_app}"))
                .andExpect(status().isOk)
                .andExpect(content().json(object_mapper.writeValueAsString(app_1)))

        Mockito.`when`(appService.getApplication(0L))
                .thenThrow(NotFoundException("No Application was found.","test on getApplication"))
        mvc.perform(get("$URL_PATH/0"))
                .andExpect(status().isNotFound)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on deleteApplication`(){
        Mockito.`when`(appService.deleteApplication(0L))
                .thenThrow(NotFoundException("No Application was found", "test on deleteApplication"))
        mvc.perform(delete("$URL_PATH/0").header("Authorization",token))
                .andExpect(status().isNotFound)

        Mockito.doNothing().doThrow(NotFoundException("No Application was found", "test on deleteApplication"))
                .`when`(appService).deleteApplication(app_2.id_app)

        mvc.perform(delete("$URL_PATH/${app_2.id_app}").header("Authorization",token))
                .andExpect(status().isNoContent)
        Mockito.verify(appService, times(1)).deleteApplication(app_2.id_app)

        mvc.perform(delete("$URL_PATH/${app_2.id_app}").header("Authorization",token))
                .andExpect(status().isNotFound)
        Mockito.verify(appService, times(2)).deleteApplication(app_2.id_app)

    }
}