package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.institution

import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.InstitutionDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.InstitutionDAO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.InstitutionService
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.web.context.WebApplicationContext
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Institution.Companion.INSTITUTION_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Student.Companion.STUDENT_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Reviewer.Companion.REVIEWER_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.StudentDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.grantcall.GrantCallControllerTest
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.JwtUtils
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.SecurityService
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.UserService

@SpringBootTest
@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
class InstitutionControllerTest {

    @Autowired
    lateinit var context :WebApplicationContext

    @Autowired
    lateinit var mvc:MockMvc

    @MockBean
    lateinit var insts:InstitutionService

    @MockBean
    lateinit var userServ:UserService

    @MockBean
    lateinit var secureServ:SecurityService

    companion object {
        val inst1 = InstitutionDAO("FCT", 1234567, "Monte da Caparica", "919243123")
        val inst2 = InstitutionDAO("IPS", 3454444, "Setúbal", "23123124")
        val inst3 = InstitutionDAO("IST", 4949234, "Lisboa", "911")
        val stud1 = StudentDTO(1,"joao peres","jp@fct.unl.pt",1,"setubal","1213")
        val stud2 = StudentDTO(2,"joao oliveira","jgcd@fct.unl.pt",2,"caparica","1213")
        val stud3 = StudentDTO(3,"joao peres","jp@fct.unl.pt",3,"lisboa","1213")
        val stud4 = StudentDTO(4,"luis silva","jp@fct.unl.pt",1,"lisboa","1213")
        val studsInst1 = setOf(stud1, stud4)

        private val rev1 = ReviewerDTO(1,"test1","test",1,"caparica","1213231")
        private val rev2 = ReviewerDTO(2,"test2","test2",1,"caparica","1213231")
        val revInst1 = setOf(rev1, rev2)

        val pageable:Pageable = PageRequest.of(0,15)

        val institutionsDAO = listOf(inst1, inst2, inst3)
        val institutionsDTO = institutionsDAO.map { InstitutionDTO(it) }
        val objmap = ObjectMapper()

        val page1 = PageImpl(institutionsDTO, pageable, institutionsDTO.size.toLong())
        val pageStuds1 = PageImpl(studsInst1.toList(), pageable, studsInst1.size.toLong())
        val pageStuds2 = PageImpl(listOf(stud2), pageable,1)
        val pageStuds3 = PageImpl(listOf(stud3), pageable,1)
        val pageRev1 = PageImpl(revInst1.toList(), pageable, revInst1.size.toLong())
        const val id:Long = 1

        val token = JwtUtils.mockJwt("test","ROLE_ADMIN")
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun getAllInstitutionsTest(){
        Mockito.`when`(insts.getAllInstitutions(0,15, emptyList())).thenReturn(page1)
        mvc.perform(get("/$RESOURCE/$INSTITUTION_PATH")
                .header("Authorization", token))
                .andExpect(status().isOk)
                //.andExpect(jsonPath("$",hasSize<Any>(institutionsDAO.size)))
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun getInstitutionTest(){
        Mockito.`when`(insts.getInstitution(id)).thenReturn(InstitutionDTO(inst1))
        mvc.perform(get("/$RESOURCE/$INSTITUTION_PATH/{id}","$id")
                .header("Authorization", token))
                .andExpect(status().isOk)
                .andExpect(content().json(objmap.writeValueAsString(InstitutionDTO(inst1))))

        Mockito.`when`(insts.getInstitution(3)).thenReturn(InstitutionDTO(inst3))
        mvc.perform(get("/$RESOURCE/$INSTITUTION_PATH/{id}",3L)
                .header("Authorization", token))
                .andExpect(status().isOk)
                .andExpect(content().json(objmap.writeValueAsString(InstitutionDTO(inst3))))

        // Test non existing institution
        Mockito.`when`(insts.getInstitution(4)).thenThrow(NotFoundException("No Institution found","test"))
        mvc.perform(get("/$RESOURCE/$INSTITUTION_PATH/{id}",4L)
                .header("Authorization", token))
                .andExpect(status().isNotFound)
    }


    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun createInstitutionTest(){
        Mockito.`when`(insts.createInstitution(institutionsDTO[0])).thenReturn(institutionsDTO[0])
        mvc.perform(post("/$RESOURCE/$INSTITUTION_PATH")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objmap.writeValueAsString(institutionsDTO[0])))
                .andExpect(status().isCreated)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun deleteInstitutionTest(){
        //Throws exception after second call
        Mockito.doNothing().doThrow(NotFoundException("Institution not found","Test"))
                .`when`(insts).deleteInstitution(id)

        mvc.perform(delete("/$RESOURCE/$INSTITUTION_PATH/{id}","$id")
                .header("Authorization", token))
                .andExpect(status().isNoContent)
        Mockito.verify(insts,times(1)).deleteInstitution(id)

        mvc.perform(delete("/$RESOURCE/$INSTITUTION_PATH/{id}","$id")
                .header("Authorization", token))
                .andExpect(status().isNotFound)
        Mockito.verify(insts,times(2)).deleteInstitution(id)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun getInstitutionStudentsTest(){
        Mockito.`when`(insts.getInstitutionStudents(id,0,15, emptyList()))
                .thenReturn(pageStuds1)
        mvc.perform(get("/$RESOURCE/$INSTITUTION_PATH/{id}/$STUDENT_PATH","$id")
                .header("Authorization", token))
                .andExpect(status().isOk)
                //.andExpect(jsonPath("$",hasSize<Any>(studsInst1.size)))

        Mockito.`when`(insts.getInstitutionStudents(2L,0,15, emptyList()))
                .thenReturn(pageStuds2)
        mvc.perform(get("/$RESOURCE/$INSTITUTION_PATH/{id}/$STUDENT_PATH",2L)
                .header("Authorization", token))
                .andExpect(status().isOk)
                //.andExpect(jsonPath("$",hasSize<Any>(1)))

        Mockito.`when`(insts.getInstitutionStudents(3L,0,15, emptyList()))
                .thenReturn(pageStuds3)
        mvc.perform(get("/$RESOURCE/$INSTITUTION_PATH/{id}/$STUDENT_PATH",3L)
                .header("Authorization", token))
                .andExpect(status().isOk)
                //.andExpect(jsonPath("$",hasSize<Any>(1)))

        Mockito.`when`(insts.getInstitutionStudents(4L,0,10, null))
                .thenThrow(NotFoundException("No Institution found","Test"))
        mvc.perform(get("/$RESOURCE/$INSTITUTION_PATH/{id}/$STUDENT_PATH",4L)
                .header("Authorization", token))
                .andExpect(status().isNotFound)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun getInstitutionReviewersTest(){
        Mockito.`when`(insts.getInstitutionReviewers(id,0,15, emptyList()))
                .thenReturn(pageRev1)
        mvc.perform(get("/$RESOURCE/$INSTITUTION_PATH/{id}/$REVIEWER_PATH","$id")
                .header("Authorization", token))
                .andExpect(status().isOk)
                //.andExpect(jsonPath("$",hasSize<Any>(revInst1.size)))

        Mockito.`when`(insts.getInstitutionReviewers(4L,0,10, null))
                .thenThrow(NotFoundException("No Institution found","Test"))
        mvc.perform(get("/$RESOURCE/$INSTITUTION_PATH/{id}/$REVIEWER_PATH",4L)
                .header("Authorization", token))
                .andExpect(status().isNotFound)
    }


    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun addStudentTest(){
        studsInst1.forEach{
            Mockito.`when`(secureServ.relationshipIsSame(1,it.idInst)).thenReturn(true)
            Mockito.`when`(userServ.addStudent(it, "passwordy")).thenReturn(it)
            mvc.perform(
                        multipart("/$RESOURCE/$INSTITUTION_PATH/1/$STUDENT_PATH")
                            .file(MockMultipartFile("stud", "stud", MediaType.APPLICATION_JSON_VALUE, objmap.writeValueAsBytes(it)))
                            .file(MockMultipartFile("password", "password", MediaType.TEXT_PLAIN_VALUE, objmap.writeValueAsBytes("passwordy")))
                            .header("Authorization", token)
                        )
                        .andExpect(status().isCreated)
        }
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun addReviewerTest(){
        revInst1.forEach{
            Mockito.`when`(secureServ.relationshipIsSame(1,it.idInst)).thenReturn(true)
            Mockito.`when`(userServ.addReviewer(it, "passwordy")).thenReturn(it)
            mvc.perform(
                    multipart("/$RESOURCE/$INSTITUTION_PATH/1/$REVIEWER_PATH")
                            .file(MockMultipartFile("rev", "rev", MediaType.APPLICATION_JSON_VALUE, objmap.writeValueAsBytes(it)))
                            .file(MockMultipartFile("password", "password", MediaType.TEXT_PLAIN_VALUE, objmap.writeValueAsBytes("passwordy")))
                            .header("Authorization", token)
                    )
                    .andExpect(status().isCreated)
        }
    }

}