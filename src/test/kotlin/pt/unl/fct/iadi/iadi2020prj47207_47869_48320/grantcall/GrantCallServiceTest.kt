package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.grantcall

import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationPanelDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.GrantCallDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.RequestItemDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.GrantCallRepository
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.GrantCallService
import java.util.*


@ExtendWith(SpringExtension::class)
@SpringBootTest
class GrantCallServiceTest{

    @Autowired
    lateinit var gcalls:GrantCallService

    @MockBean
    lateinit var repo:GrantCallRepository

    companion object{

        private val sponsor = SponsorDAO("Gambino", "gambin@campus.pt", "923456781")

        private val call_1 = GrantCallDAO("Grant Call A-class", "Join us ;^)", "",2020F,
                Date(), Date(), sponsor)
       // val evaluation_panel_1 = EvaluationPanelDAO(11L, call_1, 0L, "A-class",
        //        emptyList<ReviewerDAO>())
        //TODO fix Date to outdated(closed call)
        private val call_2 = GrantCallDAO("Grant Call B-class", "B us!","", 2021F,
               Date(), Date(), sponsor)
       // val evaluation_panel_2 = EvaluationPanelDAO(12L, call_2, 0L, "B-class",
       //         emptyList<ReviewerDAO>())

        val calls = mutableListOf(call_1, call_2)
        val pageable = PageRequest.of(0,15)
        val page1 = PageImpl(calls, pageable,calls.size.toLong())

        private val request_item_1 = RequestItemDAO("CV", "Object-1", true, call_1)
        val request_item_2 = RequestItemDAO("CV", "Object-2", true,call_1)
        private val request_item_3 = RequestItemDAO("Certificate of something", "Object-1", false, call_1)

        val reqi_call_1 = mutableListOf(request_item_1, request_item_3)

        private val institution = InstitutionDAO("FCT", 1234567, "Monte da Caparica", "919243123")
        private val student = StudentDAO("Oliveira", "olive@fct.unl.pt", institution, "stuff", "912345678")
        private val subi = SubmissionItemDAO(1L, "CV", "Object-1", null)
        val app_1 = ApplicationDAO(Date(), ApplicationDAO.Status.ON_HOLD, student, call_1, mutableSetOf<SubmissionItemDAO>(subi))
    }

    @Test
    fun `test on getAll calls`() {
        Mockito.`when`(repo.findAll(pageable)).thenReturn(page1)

        assertEquals(gcalls.getAllGrantCalls("ALL",0,15,null), page1.map { GrantCallDTO(it) })
    }

    @Test
    fun `test on getAll calls (closed)`() {
        Mockito.`when`(repo.findAllClosedGrantCalls(pageable)).thenReturn(page1)

        assertEquals(gcalls.getAllGrantCalls("CLOSED",0,15,null), page1.map { GrantCallDTO(it) })
    }

    @Test
    fun `test on getOne call by id`() {
        Mockito.`when`(repo.findById(1L)).thenReturn(Optional.of(call_1))

        assertEquals(gcalls.getGrantCall(1L), GrantCallDTO(call_1))
    }

    //TODO: Ver isto..
    @Test
    @Disabled
    fun `test on getOne call by id (exception)`() {
        Mockito.`when`(repo.findById(anyLong())).thenThrow(
                NotFoundException("GrantCall not Found","GrantCall Test")
        )
        assertEquals(gcalls.getGrantCall(0),NotFoundException("GrantCall not Found","GrantCall Test"))
    }
    @Disabled
    @Test
    fun `test on createOne call`() {

        val request_item_1 = RequestItemDTO(1L, "CV", true, "Object-1")
        val request_item_3 = RequestItemDTO(11L, "Certificate of something", false, "Object-1")
        val reqi_call_3 = listOf(request_item_1, request_item_3)
        val call_3 = GrantCallDTO(3L, GrantCallControllerTest.sponsor.id, 1L, "Grant Call A-class", "Join us ;^)",
                "CV and Certificate of something needed.", 2020F, "2020-10-28", "2020-11-28", reqi_call_3)
        val evaluation_panel_3 = EvaluationPanelDTO(1L, call_3.id, 0L, "A-class")

        Mockito.`when`(repo.save(Mockito.any(GrantCallDAO::class.java)))
                .then {
                    val gc:GrantCallDAO = it.getArgument(0)
                    assertEquals(gc.id, call_3.id)
                    assertEquals(gc.title, call_3.title)
                    assertEquals(gc.description, call_3.description)
                    assertEquals(gc.funding, call_3.funding)
                    assertEquals(gc.open_date, call_3.open_date)
                    assertEquals(gc.deadline, call_3.deadline)
                    assertEquals(gc.sponsor.id, call_3.id_sponsor)
                    assertEquals(gc.requestItems, call_3.reqItems)
                    gc
                }
        //gcalls.createGrantCall(call_3,evaluation_panel_3)

        Mockito.`when`(repo.findById(1L)).thenReturn(Optional.of(call_1));

        assertEquals(gcalls.getGrantCall(1L), call_1)
    }
}