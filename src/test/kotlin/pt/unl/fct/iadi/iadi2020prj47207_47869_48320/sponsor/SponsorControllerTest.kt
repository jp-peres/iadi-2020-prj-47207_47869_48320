package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.sponsor

import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockPart
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.context.junit.jupiter.SpringExtension
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.GrantCall.Companion.GRANTCALL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Sponsor.Companion.SPONSOR_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.GrantCallDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.RequestItemDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.SponsorDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.SponsorService
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.institution.InstitutionControllerTest
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.JwtUtils
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.SecurityService

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
class SponsorControllerTest {

    @MockBean
    lateinit var sService: SponsorService

    @MockBean
    lateinit var secureServ: SecurityService

    @Autowired
    lateinit var mvc: MockMvc

    companion object{
        private var object_mapper = ObjectMapper()

        const val URL_PATH = "/$RESOURCE/$SPONSOR_PATH"

        private val request_item_1 = RequestItemDTO(1L, "CV", true, "Object-1")
        private val request_item_2 = RequestItemDTO(2L, "CV", true, "Object-2")
        private val request_item_3 = RequestItemDTO(11L, "Certificate of something", false, "Object-1")
        private val request_item_4 = RequestItemDTO(21L, "Demo", false, "Object-2")
        private val request_item_5 = RequestItemDTO(3L, "CV", true, "Object-3")

        private val reqi_call_1 = listOf(request_item_1, request_item_3)
        private val reqi_call_2 = listOf(request_item_2, request_item_4)
        private val reqi_call_3 = listOf(request_item_5)

        private val call_1 = GrantCallDTO(1L, 1L, 1L, "Grant Call A-class", "Join us ;^)",
                "CV and Certificate of something needed.", 2020F, "2020-10-28", "2020-11-28", reqi_call_1)

        private val call_2 = GrantCallDTO(2L, 1L, 2L, "Grant Call B-class", "B us!",
                "CV needed", 2021F, "2020-09-28", "2020-10-28", reqi_call_2)

        private val call_3 = GrantCallDTO(3L, 1L, 3L, "Grant Call C-class", "C to update!",
                "CV needed", 2022F, "2020-11-28", "2020-12-28", reqi_call_3)

        private val calls = listOf(call_1, call_2, call_3)

        val sponsor = SponsorDTO(1L,"Gambino", "gambin@campus.pt", "923456781", calls)

        val sponsor_2 = SponsorDTO(2L,"Stuart", "stuart@campus.pt", "934567892", emptyList())

        val sponsor_3 = SponsorDTO(3L,"Bob", "bob@campus.pt", "915678903", emptyList())

        private var sponsors = mutableListOf<SponsorDTO>(sponsor, sponsor_2)

        private val pageable = PageRequest.of(0,15)
        val pageSpo = PageImpl(sponsors, pageable,2)
        val pageCalls = PageImpl(sponsor.grantCalls, pageable,sponsor.grantCalls.size.toLong())

        val token = JwtUtils.mockJwt("test","ROLE_ADMIN")
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on getSponsors`(){
        Mockito.`when`(sService.getSponsors(0,10,null))
                .thenReturn(pageSpo)
        mvc.perform(get(URL_PATH).header("Authorization", token))
                .andExpect(status().isOk)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on getSponsor`(){
        Mockito.`when`(sService.getSponsor(sponsor.id))
                .thenReturn(sponsor)
        mvc.perform(get("$URL_PATH/${sponsor.id}").header("Authorization", token))
                .andExpect(status().isOk)
                .andExpect(content().json(object_mapper.writeValueAsString(sponsor)))

        Mockito.`when`(sService.getSponsor(0L))
                .thenThrow(NotFoundException("No Sponsor was found.","test on getSponsor"))
        mvc.perform(get("$URL_PATH/0").header("Authorization", token))
                .andExpect(status().isNotFound)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on getSponsorGrantCalls`(){
        Mockito.`when`(sService.getSponsorGrantCalls(sponsor.id,0,10,null))
                .thenReturn(pageCalls)
        mvc.perform(get("$URL_PATH/${sponsor.id}/$GRANTCALL_PATH")
                .header("Authorization", token))
                .andExpect(status().isOk)

        Mockito.`when`(sService.getSponsorGrantCalls(0L,0,10,null))
                .thenThrow(NotFoundException("No Sponsor Found","test on getSponsorGrantCalls"))
        mvc.perform(get("$URL_PATH/0/$GRANTCALL_PATH")
                .header("Authorization", token))
                .andExpect(status().isNotFound)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on createSponsor`(){
        Mockito.`when`(sService.createSponsor(sponsor_2, "password"))
                .thenReturn(sponsor_2)
        val part1 = MockPart("sponsor","sponsor", object_mapper.writeValueAsString(sponsor_2).toByteArray())
        part1.headers.contentType = MediaType.APPLICATION_JSON
        val part2 = MockPart("password","password","password".toByteArray())
        part2.headers.contentType = MediaType.TEXT_PLAIN
        // multipart does implicitly the post method
        mvc.perform(multipart("$URL_PATH")
                .part(part1)
                .part(part2)
                .header("Authorization", token))
                .andExpect(status().isCreated)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on updateSponsor`(){
        Mockito.`when`(sService.updateSponsor(0L, sponsor_3))
                .thenThrow(NotFoundException("No Sponsor Found", "test on updateSponsor"))

        mvc.perform(put("$URL_PATH/0")
                .contentType(MediaType.APPLICATION_JSON)
                .content(object_mapper.writeValueAsString(sponsor_3))
                .header("Authorization", token))
                .andExpect(status().isNotFound)

        mvc.perform(put("$URL_PATH/${sponsor_2.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(object_mapper.writeValueAsString(sponsor_3))
                .header("Authorization", token))
                .andExpect(status().isOk)

    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on deleteSponsor`(){
        Mockito.`when`(sService.deleteSponsor(0L))
                .thenThrow(NotFoundException("No Sponsor Found", "test on updateSponsor"))
        mvc.perform(delete("$URL_PATH/0").header("Authorization", token))
                .andExpect(status().isNotFound)


        Mockito.doNothing().doThrow(NotFoundException("No Sponsor Found", "test on updateSponsor"))
                .`when`(sService).deleteSponsor(sponsor_2.id)
        mvc.perform(delete("$URL_PATH/${sponsor_2.id}").header("Authorization", token))
                .andExpect(status().isNoContent)
        Mockito.verify(sService, times(1)).deleteSponsor(sponsor_2.id)

        mvc.perform(delete("$URL_PATH/${sponsor_2.id}").header("Authorization", token))
                .andExpect(status().isNotFound)
        Mockito.verify(sService, times(2)).deleteSponsor(sponsor_2.id)
    }
}