package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.application

import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.repositories.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.users.UserRepositoryTest
import java.util.*

@SpringBootTest
@ExtendWith(SpringExtension::class)
class ApplicationRepositoryTest {

    @Autowired
    lateinit var instRepo: InstitutionRepository

    @Autowired
    lateinit var studRepo: StudentRepository

    @Autowired
    lateinit var appRepo: ApplicationRepository

    @Autowired
    lateinit var grantRepo: GrantCallRepository

    @Autowired
    lateinit var spanRepo: SponsorRepository

    companion object{
        var inst1 = InstitutionDAO("FCT",420, "Cap","N/A")
        var student1 = StudentDAO("bica", "bica@bica.bica", UserRepositoryTest.inst1, "Entrocar", "9177")
        var student2 = StudentDAO("tiago", "tiago@tiago.tiago", UserRepositoryTest.inst2, "Enbarqar", "9669")
        var sponsor1 = SponsorDAO("peres", "peres@peres.peres", "48320")
        var sponsor2 = SponsorDAO("joao", "joao@joao.joao", "47869")
        var grant1 = GrantCallDAO("title1","description1", "N/A", 0f, Date(),Date(), sponsor1)
        var grant2 = GrantCallDAO("title2","description2", "N/A", 0f, Date(),Date(), sponsor2)
        var grant3 = GrantCallDAO("title3","description3", "N/A", 0f, Date(),Date(), sponsor1)
        var app1 = ApplicationDAO(Date(),ApplicationDAO.Status.ON_HOLD, student1, grant1, emptySet())
        var app2 = ApplicationDAO(Date(),ApplicationDAO.Status.ON_HOLD, student2, grant1, emptySet())
        var app3 = ApplicationDAO(Date(),ApplicationDAO.Status.ON_HOLD, student1, grant2, emptySet())
        var app4 = ApplicationDAO(Date(),ApplicationDAO.Status.ON_HOLD, student1, grant3, emptySet())

    }

    @BeforeEach
    @Transactional
    fun `prepare for test`(){
        inst1 = InstitutionDAO("FCT",420, "Cap","N/A")
        inst1 = instRepo.save(inst1)

        student1 = StudentDAO("bica", "bica@bica.bica", inst1, "Entrocar", "9177")
        student2 = StudentDAO("tiago", "tiago@tiago.tiago", inst1, "Enbarqar", "9669")
        student1 = studRepo.save(student1)
        student2 = studRepo.save(student2)
        inst1.users.addAll(setOf(student1, student2))

        sponsor1 = SponsorDAO("peres", "peres@peres.peres", "48320")
        sponsor2 = SponsorDAO("joao", "joao@joao.joao", "47869")
        sponsor1 = spanRepo.save(sponsor1)
        sponsor2 = spanRepo.save(sponsor2)

        grant1 = GrantCallDAO("title1","description1", "N/A", 0f, Date(),Date(), sponsor1)
        grant2 = GrantCallDAO("title2","description2", "N/A", 0f, Date(),Date(), sponsor2)
        grant3 = GrantCallDAO("title3","description3", "N/A", 0f, Date(),Date(), sponsor1)
        grant1 = grantRepo.save(grant1)
        grant2 = grantRepo.save(grant2)
        grant3 = grantRepo.save(grant3)
        sponsor1.grantCalls.addAll(setOf(grant1, grant3))
        sponsor2.grantCalls.add(grant2)

        app1 = ApplicationDAO(Date(),ApplicationDAO.Status.ON_HOLD, student1, grant1, emptySet())
        app2 = ApplicationDAO(Date(),ApplicationDAO.Status.ON_HOLD, student2, grant1, emptySet())
        app3 = ApplicationDAO(Date(),ApplicationDAO.Status.ON_HOLD, student1, grant2, emptySet())
        app4 = ApplicationDAO(Date(),ApplicationDAO.Status.ON_HOLD, student1, grant3, emptySet())

    }

    @AfterEach
    @Transactional
    fun `clean up test`(){
        appRepo.deleteAll()
        grantRepo.deleteAll()
        spanRepo.deleteAll()
        studRepo.deleteAll()
        instRepo.deleteAll()
    }

    @Test
    @Transactional
    fun `insert & get applications`(){
        assertEquals(0,appRepo.count())
        val id1 = appRepo.save(app1).app_id
        assertEquals(true, appRepo.existsById(id1))
        val id2 = appRepo.save(app2).app_id
        assertEquals(true, appRepo.existsById(id2))
        assertEquals(2,appRepo.count())
    }

    @Test
    //@Disabled
    @Transactional
    fun `delete applications`(){
        assertEquals(0,appRepo.count())
        app1 = appRepo.save(app1)
        assertEquals(1,appRepo.count())
        appRepo.deleteById(app1.app_id)
        assertEquals(false, appRepo.existsById(app1.app_id))
        app1 = appRepo.save(app1)
        app2 = appRepo.save(app2)
        app3 = appRepo.save(app3)
        app4 = appRepo.save(app4)
        assertEquals(4,appRepo.count())

        appRepo.deleteById(app1.app_id)
        assertEquals(3,appRepo.count())
        appRepo.deleteById(app2.app_id)
        assertEquals(2,appRepo.count())
        appRepo.deleteById(app3.app_id)
        assertEquals(1,appRepo.count())
        appRepo.deleteById(app4.app_id)
        assertEquals(0,appRepo.count())
    }

    @Test
    @Transactional
    fun `get all applications`(){
        assertEquals(0,appRepo.count())
        app1 = appRepo.save(app1)
        app2 = appRepo.save(app2)
        app3 = appRepo.save(app3)
        app4 = appRepo.save(app4)

        val allApps = appRepo.findAll()
        assertEquals(4,allApps.size)
    }

    @Test
    @Transactional
    fun `get application`() {
        assertEquals(0, appRepo.count())
        val stored = appRepo.save(app1)
        assertEquals(stored.grant_call.id,app1.grant_call.id)
        assertNotEquals(stored.grant_call.id,app3.grant_call.id)
    }
}