package pt.unl.fct.iadi.iadi2020prj47207_47869_48320.grantcall

import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.Iadi2020Prj472074786948320Application.Companion.RESOURCE
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.EvaluationPanel.Companion.EVALUATION_PANEL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.GrantCall.Companion.GRANTCALL_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.Student.Companion.STUDENT_PATH
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.EvaluationPanelDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.GrantCallDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.RequestItemDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.api.dto.ReviewerDTO
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.application.ApplicationControllerTest
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.exception.NotFoundException
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.model.dao.*
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.security.JwtUtils
import pt.unl.fct.iadi.iadi2020prj47207_47869_48320.services.GrantCallService

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
class GrantCallControllerTest {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var gcall: GrantCallService

    companion object {

        var object_mapper = ObjectMapper()

        const val URL_PATH:String = "/$RESOURCE/$GRANTCALL_PATH"

        val sponsor = SponsorDAO("Gambino", "gambin@campus.pt", "923456781")

        val request_item_1 = RequestItemDTO(1L, "CV", true, "Object-1")
        val request_item_2 = RequestItemDTO(2L, "CV", true, "Object-2")
        val request_item_3 = RequestItemDTO(11L, "Certificate of something", false, "Object-1")
        val request_item_4 = RequestItemDTO(12L, "Demo", false, "Object-1")
        val request_item_5 = RequestItemDTO(3L, "CV", true, "Object-3")

        val reqi_call_1 = listOf(request_item_1, request_item_3)

        val call_1 = GrantCallDTO(1L, sponsor.id, 1L, "Grant Call A-class", "Join us ;^)",
                "CV and Certificate of something needed.", 2020F, "2020-10-28", "2020-11-28", reqi_call_1)

        val call_2 = GrantCallDTO(2L, sponsor.id, 2L, "Grant Call B-class", "B us!",
                "CV needed", 2021F, "2020-09-28", "2020-10-28", listOf(request_item_2))

        val call_3 = GrantCallDTO(3L, sponsor.id, 3L, "Grant Call C-class", "C to update!",
                "CV needed", 2022F, "2020-11-28", "2020-12-28", listOf(request_item_5))

        val calls = listOf(call_1, call_2, call_3)

        val pageable = PageRequest.of(0,15)

        val callsPage1 = PageImpl(calls, pageable,calls.size.toLong())
        val callsPage2 = PageImpl(listOf(call_3), pageable,1)
        val callsPage3 = PageImpl(listOf(call_1, call_2), pageable,2)

        val evaluation_panel_1 = EvaluationPanelDTO(11L, call_1.id, 0L, "A-class")

        val rev1 = ReviewerDTO(1,"test1","test",1,"caparica","1213231")
        val rev2 = ReviewerDTO(2,"test2","test2",1,"caparica","1213231")
        val rev_call_1 = listOf(rev1.id, rev2.id)

        val token = JwtUtils.mockJwt("test", "ROLE_ADMIN")
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on getAll calls`() {
        Mockito.`when`(gcall.getAllGrantCalls(null,0,15,null))
                .thenReturn(callsPage1)
        mvc.perform(get(URL_PATH).header("Authorization", token))
                .andExpect(status().isOk)

        Mockito.`when`(gcall.getAllGrantCalls("ALL",0,15,null))
                .thenReturn(callsPage1)
        mvc.perform(get("$URL_PATH?status=ALL").header("Authorization", token))
                .andExpect(status().isOk)

    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on getAll calls(closed)`() {
        Mockito.`when`(gcall.getAllGrantCalls("CLOSED",0,15,null))
                .thenReturn(callsPage2)
        mvc.perform(get("$URL_PATH?status=CLOSED").header("Authorization", token))
                .andExpect(status().isOk)
                //.andExpect(jsonPath("$", hasSize<Any>(calls.subList(1,2).size)))
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on getAll calls(open) from student`(){
        Mockito.`when`(gcall.getAllGrantCalls("OPEN",0,15,null))
                .thenReturn(callsPage3)
        mvc.perform(get("$URL_PATH/$STUDENT_PATH").header("Authorization", token))
                .andExpect(status().isOk)
                //.andExpect(jsonPath("$", hasSize<Any>(calls.subList(0,1).size)))
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on getPanel by call id`(){
        Mockito.`when`(gcall.getEvalPanelCall(call_1.id))
                .thenReturn(evaluation_panel_1)
        mvc.perform(get("$URL_PATH/${call_1.id}/$EVALUATION_PANEL_PATH").header("Authorization", token))
                .andExpect(status().isOk)
                .andExpect(content().json(object_mapper.writeValueAsString(evaluation_panel_1)))

        Mockito.`when`(gcall.getEvalPanelCall(0L))
                .thenThrow(NotFoundException("No GrantCall was found.","test on getPanel by call id"))
        mvc.perform(get("$URL_PATH/0/$EVALUATION_PANEL_PATH").header("Authorization", token))
                .andExpect(status().isNotFound)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on getCall by call id`(){
        Mockito.`when`(gcall.getGrantCall(call_2.id))
                .thenReturn(call_2)
        mvc.perform(get("$URL_PATH/${call_2.id}").header("Authorization", token))
                .andExpect(status().isOk)
                .andExpect(content().json(object_mapper.writeValueAsString(call_2)))

        Mockito.`when`(gcall.getGrantCall(0L))
                .thenThrow(NotFoundException("No GrantCall was found.","test on getCall by call id"))
        mvc.perform(get("$URL_PATH/0").header("Authorization", token))
                .andExpect(status().isNotFound)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on updateCall`(){
        Mockito.doNothing().`when`(gcall).updateGrantCall(call_3.id, call_1)
        mvc.perform(put("$URL_PATH/${call_3.id}").header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(object_mapper.writeValueAsString(call_1)))
                .andExpect(status().isNoContent)

        Mockito.`when`(gcall.updateGrantCall(0L, call_1))
                .thenThrow(NotFoundException("No GrantCall was found.","test on getCall by call id"))
        mvc.perform(put("$URL_PATH/0").header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(object_mapper.writeValueAsString(call_1)))
                .andExpect(status().isNotFound)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on deleteCall`(){
        Mockito.doNothing().doThrow(NotFoundException("No GrantCall Found","test on deleteCall"))
                .`when`(gcall).deleteGrantCall(call_2.id)

        mvc.perform(delete("$URL_PATH/${call_2.id}").header("Authorization", token))
                .andExpect(status().isNoContent)
        Mockito.verify(gcall, times(1)).deleteGrantCall(call_2.id)


        mvc.perform(delete("$URL_PATH/${call_2.id}").header("Authorization", token))
                .andExpect(status().isNotFound)
        Mockito.verify(gcall, times(2)).deleteGrantCall(call_2.id)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on addRequest items`(){
        Mockito.`when`(gcall.addRequestItem(call_1.id, request_item_4))
                .thenReturn(request_item_4)
        mvc.perform(post("$URL_PATH/${call_1.id}/items").header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(object_mapper.writeValueAsString(request_item_4)))
                .andExpect(status().isOk)

        Mockito.`when`(gcall.addRequestItem(call_1.id, request_item_4))
                .thenThrow(NotFoundException("No GrantCall was found.","test on addRequest items"))
        mvc.perform(post("$URL_PATH/${call_1.id}/items").header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(object_mapper.writeValueAsString(request_item_4)))
                .andExpect(status().isNotFound)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on deleteRequest item`(){
        Mockito.doNothing().doThrow(NotFoundException("No RequestItem Found","test on deleteRequest item"))
                .`when`(gcall).deleteRequestItem(call_1.id, request_item_3.id)

        mvc.perform(delete("$URL_PATH/${call_1.id}/items/${request_item_3.id}")
                .header("Authorization", token))
                .andExpect(status().isNoContent)
        Mockito.verify(gcall, times(1)).deleteRequestItem(call_1.id, request_item_3.id)
        mvc.perform(delete("$URL_PATH/${call_1.id}/items/${request_item_3.id}")
                .header("Authorization", token))
                .andExpect(status().isNotFound)
        Mockito.verify(gcall, times(2)).deleteRequestItem(call_1.id, request_item_3.id)
    }

    @WithMockUser(username = "test", password = "test", roles = ["ADMIN"])
    @Test
    fun `test on getRequest by call id`(){
        Mockito.`when`(gcall.getRequestItems(call_1.id))
                .thenReturn(reqi_call_1)

        mvc.perform(get("$URL_PATH/${call_1.id}/items").header("Authorization", token))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$", hasSize<Any>(reqi_call_1.size)))

        Mockito.`when`(gcall.getRequestItems(call_1.id))
                .thenThrow(NotFoundException("No GrantCall was found.","test on getRequest by call id"))
        mvc.perform(get("$URL_PATH/${call_1.id}/items").header("Authorization", token))
                .andExpect(status().isNotFound)
    }
}

/*
        private val request_item_1 = RequestItemDAO(1L, "CV", "Object-1", true, call_1)
        private val request_item_2 = RequestItemDAO(2L, "CV", "Object-2", true, call_2)
        private val request_item_3 = RequestItemDAO(12L, "Certificate of something", "Object-1", false, call_1)

        private val reqi_call_1 = listOf(request_item_1, request_item_3)

        private val institution = InstitutionDAO("FCT", 1234567, "Monte da Caparica", "919243123")
        private val student = StudentDAO(1L, StudentDTO.STUDENT_TYPE, "Oliveira", "olive@fct.unl.pt", institution, "stuff", "912345678", emptySet())
        private val subi = SubmissionItemDAO(1L, "CV", "Object-1", null)
        private val app_1 = ApplicationDAO(1L, Date(), "under analysis", student, call_1, mutableSetOf<SubmissionItemDAO>(subi))
*/