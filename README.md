﻿# IADI Project

## Project By

```
    João Oliveira - nº 47869
    João Peres - nº 48320
    Tiago Bica - nº 47207
```

## Project Progress

 * Controller Tests not working anymore due to spring security integration;
 * Grant Applications functionalities not fully implemented;
 * Grant Call Service missing fuctionality to get Grant Applications by call id;
 * Role based authorization is implemented but lacking user for now (database is not persistent yet);
 * Some unit test are missing or not fully fledged;
 * JWT Auth was integrated (each request now requires a valid JWT with credentials);

### Extra Notes

 * Use a post request with url as /login having a body as json with the following:
```
{
    "username": "test", 
    "password": "test"
}
```
This will generate the respective JWT Token for future requests for authentication use.
